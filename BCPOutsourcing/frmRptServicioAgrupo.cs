﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BCPOutsourcing
{
    public partial class frmRptServicioAgrupo : Form
    {
        public frmRptServicioAgrupo()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Excel Documents (*.xls)|*.xls";
                sfd.FileName = "exportar.xls";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    ToCsV(dataGridView1, sfd.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ToCsV(DataGridView dGV, string filename)
        {
            string stOutput = "";
            // Export titles:
            string sHeaders = "";

            for (int j = 0; j < dGV.Columns.Count; j++)
                sHeaders = sHeaders.ToString() + Convert.ToString(dGV.Columns[j].HeaderText) + "\t";
            stOutput += sHeaders + "\r\n";
            // Export data.
            for (int i = 0; i < dGV.RowCount - 1; i++)
            {
                string stLine = "";
                for (int j = 0; j < dGV.Rows[i].Cells.Count; j++)
                    stLine = stLine.ToString() + Convert.ToString(dGV.Rows[i].Cells[j].Value) + "\t";
                stOutput += stLine + "\r\n";
            }
            Encoding utf16 = Encoding.GetEncoding(1254);
            byte[] output = utf16.GetBytes(stOutput);
            FileStream fs = new FileStream(filename, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(output, 0, output.Length); //write the encoded file
            bw.Flush();
            bw.Close();
            fs.Close();
        }

        private void frmRptServicioAgrupo_Load(object sender, EventArgs e)
        {
            CargarArea();
        }
        void CargarArea()
        {
            BaseDato b = new BaseDato();
            b.Ejecutar("select * FROM area", BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            for (int i = 0; i < b.Tabla.Rows.Count; i++)
            {
                comboBox1.ValueMember = "idarea";
                comboBox1.DisplayMember = "nombre";
                comboBox1.DataSource = b.Tabla;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CargarRequerimientoServicio();
            }
            catch (Exception ex )
            {
                MessageBox.Show(ex.Message);
            }
            
        }



        public void CargarRequerimientoServicio()
        {
            string sql = "" +
                "SELECT " +
                " Estados Estado, " +
                " fechasolicitud as 'Fecha de Solicitud', " +
                " areasolicitante 'Area Solicitante', " +
                " Motivo, " +
                " Detalle, " +
                " Solicitante, " +
                " GSTI, " +
                " Outsourcing " +
                " FROM( " +
                    "SELECT " +
                    "CASE " +
                    "    WHEN(EXISTS(SELECT 1 FROM[dbo].[PCR][p] " +
                    "                    INNER JOIN[dbo].[Servicio][s] ON[s].[idservicio] = [p].[idpcr]  " +
                    "                    WHERE[p].[idrequerimiento] =[r].[idrequerimiento])) THEN 'Concluidos' " +
                    "    WHEN(EXISTS(SELECT 1 FROM[dbo].[PCR][p] WHERE[p].[idrequerimiento] =[r].[idrequerimiento] AND[p].[activo] = 'A')) THEN 'PCR generado' " +
                    "    WHEN([r].[estado] = 'RO') THEN 'Requerimiento Rechazado por Outsourcing' " +
                    "    WHEN([r].[estado] = 'RG') THEN 'Requerimiento Rechazado por GSTI' " +
                    "    WHEN([r].[idusuarioOutsourcing] IS NOT NULL) THEN 'Pendiente por generar PCR' " +
                    "    WHEN([r].[idusuarioGSTI] IS NULL) THEN 'Pendiente de validacion por GSTI' " +
                    "    WHEN([r].[idusuarioGSTI] IS NOT NULL) THEN 'Pendiente de validacion por Outsourcing' " +
                    "END 'Estados', " +
                    "u.usuario solicitante, " +
                    "a.nombre areasolicitante, " +
                    "u1.usuario gsti, " +
                    "u2.usuario Outsourcing, " +
                    " fechasolicitud, " +
                    " Motivo, " +
                    " Detalle " +
                    "FROM [dbo].[Requerimiento][r]" +
                    "    INNER JOIN [dbo].[Usuario] [u] ON [r].[idusuariosolicitante] = [u].[idusuario] " +
                    "    INNER JOIN [dbo].[area] [a] ON [a].[idarea] = [u].[idarea] " +
                    "    left JOIN [dbo].[Usuario] [u1] ON [r].[idusuarioGSTI] = [u1].[idusuario] " +
                    "    left JOIN [dbo].[Usuario] [u2] ON [r].[idusuarioOutsourcing] = [u2].[idusuario]" +
                    ") AS t " +
                    "WHERE [t].[areasolicitante] LIKE CASE WHEN(@area = 'Todos') THEN '%' ELSE @area END " +
                    "and t.fechasolicitud between @fecha1 and (@fecha2 + 1) ";

            BaseDato b = new BaseDato();
            b.AgregarParametro("@area", comboBox1.Text);
            b.AgregarParametro("@fecha1", dateTimePicker1.Value);
            b.AgregarParametro("@fecha2", dateTimePicker2.Value);
            b.Ejecutar(sql, BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            dataGridView1.DataSource = b.Tabla;
            dataGridView1.Refresh();
        }


    }


}
