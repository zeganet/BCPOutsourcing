﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BCPOutsourcing
{
    public partial class frmCategoriaList : Form
    {
        public frmCategoriaList()
        {
            InitializeComponent();
        }

        private void frmCategoria_Load(object sender, EventArgs e)
        {
            CargaCategoria();
        }
        public void CargaCategoria()
        {
            BaseDato b = new BaseDato();
            string sql;
            sql = "select  'Editar' 'Editar', idcategoria,nombre from categoria ";

            b.Ejecutar(sql, BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            dataGridView1.DataSource = b.Tabla;
            dataGridView1.Refresh();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                frmCategoria frm = new frmCategoria();
                frm.MdiParent = this.MdiParent;
                frm.idCategoria = int.Parse(dataGridView1.Rows[e.RowIndex].Cells["idcategoria"].Value.ToString());
                frm.Nombre = dataGridView1.Rows[e.RowIndex].Cells["nombre"].Value.ToString();
                frm.Show();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmCategoria frm = new frmCategoria();
            frm.MdiParent = this.MdiParent;
            frm.Nombre = "";
            frm.idCategoria = 0;
            frm.Show();
        }
    }
}
