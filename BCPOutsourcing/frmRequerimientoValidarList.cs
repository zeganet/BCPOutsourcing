﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BCPOutsourcing
{
    public partial class frmRequerimientoValidarList : Form
    {
        public frmRequerimientoValidarList()
        {
            InitializeComponent();
        }

        private void frmRequerimientoValidarList_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
            CargaRequerimientoList();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CargaRequerimientoList();
        }

        public void CargaRequerimientoList() {
            string sql = "" +
                          "SELECT " +
                          " 'Ver' 'Ver', " +
                          " Estados Estado, " +
                          " fechasolicitud as 'Fecha de Solicitud', " +
                          " Motivo, " +
                          " Detalle, " +
                          " Solicitante, " +
                          " GSTI, " +
                          " Outsourcing, " +
                          " idrequerimiento " +
                          " FROM( " +
                              "SELECT " +
                              "CASE " +
                              "    WHEN(EXISTS(SELECT 1 FROM[dbo].[PCR][p] " +
                              "                    INNER JOIN[dbo].[Servicio][s] ON[s].[idservicio] = [p].[idpcr]  " +
                              "                    WHERE[p].[idrequerimiento] =[r].[idrequerimiento])) THEN 'Concluidos' " +
                              "    WHEN(EXISTS(SELECT 1 FROM[dbo].[PCR][p] WHERE[p].[idrequerimiento] =[r].[idrequerimiento] AND[p].[activo] = 'A')) THEN 'PCR generado' " +
                              "    WHEN([r].[estado] = 'RO') THEN 'Requerimiento Rechazado por Outsourcing' " +
                              "    WHEN([r].[estado] = 'RG') THEN 'Requerimiento Rechazado por GSTI' " +
                              "    WHEN([r].[idusuarioOutsourcing] IS NOT NULL) THEN 'Pendiente por generar PCR' " +
                              "    WHEN([r].[idusuarioGSTI] IS NULL) THEN 'Pendiente de validacion por GSTI' " +
                              "    WHEN([r].[idusuarioGSTI] IS NOT NULL) THEN 'Pendiente de validacion por Outsourcing' " +
                              "END 'Estados', " +
                              "u.usuario solicitante, " +
                              "u1.usuario gsti, " +
                              "u2.usuario Outsourcing, " +
                              " fechasolicitud, " +
                              " Motivo, " +
                              " Detalle, " +
                              " idrequerimiento " +
                              "FROM [dbo].[Requerimiento][r]" +
                              "    INNER JOIN [dbo].[Usuario] [u] ON [r].[idusuariosolicitante] = [u].[idusuario] " +
                              "    left JOIN [dbo].[Usuario] [u1] ON [r].[idusuarioGSTI] = [u1].[idusuario] " +
                              "    left JOIN [dbo].[Usuario] [u2] ON [r].[idusuarioOutsourcing] = [u2].[idusuario]" +
                              ") AS t " +
                              "WHERE [t].[Estados] LIKE CASE WHEN(@estado = 'Todos') THEN '%' ELSE @estado END " +
                              "and t.motivo like '%' + @motivo + '%' ";

            BaseDato b = new BaseDato();
            b.AgregarParametro("@estado", comboBox1.Text);
            b.AgregarParametro("@motivo", textBox1.Text);
            b.Ejecutar(sql, BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            dataGridView1.DataSource = b.Tabla;
            dataGridView1.Refresh();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                if (dataGridView1.Rows[e.RowIndex].Cells["Estado"].Value.ToString().Contains("PCR generado") ||
                    dataGridView1.Rows[e.RowIndex].Cells["Estado"].Value.ToString().Contains("Concluido")) //PCR
                {
                    frmPCR frm = new frmPCR();
                    frm.idrequerimiento = int.Parse(dataGridView1.Rows[e.RowIndex].Cells["idrequerimiento"].Value.ToString());
                    frm.MdiParent = this.MdiParent;
                    frm.Show();
                }
                else {
                    frmRequerimientoServicio frm = new frmRequerimientoServicio();
                    frm.MdiParent = this.MdiParent;
                    frm.isLectura = true;
                    //if (((frmParent)Parent.Parent).Usuario.cargo.idcargo != 1)
                    //{
                        frm.idrequerimiento = int.Parse(dataGridView1.Rows[e.RowIndex].Cells["idrequerimiento"].Value.ToString());
                        frm.CargaRequerimiento();
                    //}
                    frm.Show();
                    if (((frmParent)Parent.Parent).Usuario.cargo.idcargo == 2)
                    {
                        if (!dataGridView1.Rows[e.RowIndex].Cells["Estado"].Value.ToString().Contains("GSTI"))
                        {
                            frm.Bloquear();
                        }
                    }
                    if (((frmParent)Parent.Parent).Usuario.cargo.idcargo == 3)
                    {
                        if (!dataGridView1.Rows[e.RowIndex].Cells["Estado"].Value.ToString().Contains("Outsourcing"))
                        {
                            frm.Bloquear();
                        }
                    }
                }
            }
        }
    }
}
