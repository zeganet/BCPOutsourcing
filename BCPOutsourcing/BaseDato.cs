﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;

public class BaseDato
{

    private System.Data.SqlClient.SqlConnection Cn = new System.Data.SqlClient.SqlConnection("Data Source=(local);Initial Catalog=BCPOutsourcing;Integrated Security=SSPI;");
    private SqlCommand cmd = new SqlCommand();

    SqlTransaction Trans;
    /// <summary>
    /// Muestra el Resultado de la Consulta
    /// </summary>
    public string Resultado = "";
    public bool EsError = false;
    public DataTable Tabla = new DataTable();
    public DataSet DsTablas = new DataSet();
    public enum TipoEjecucion
    {
        NoDevuelveNada = 1,
        DevuelveString = 2,
        DevuelveTable = 3,
        DevuelveDataSet = 4
    }

    public BaseDato()
    {
        cmd.Connection = Cn;
    }

    public void AgregarParametro(string nombre, object valor)
    {
        cmd.Parameters.Add(new SqlParameter(nombre, valor));
    }

    public void Ejecutar(string Consulta, TipoEjecucion Retorna = TipoEjecucion.DevuelveString, CommandType TipoComando = CommandType.StoredProcedure)
    {
        try
        {
            cmd.CommandText = Consulta;
            cmd.CommandType = TipoComando;
            if (Retorna == TipoEjecucion.NoDevuelveNada)
            {
                Cn.Open();
                Resultado = cmd.ExecuteNonQuery().ToString();
                Cn.Close();

            }
            else if (Retorna == TipoEjecucion.DevuelveString)
            {
                Cn.Open();
                Resultado = cmd.ExecuteScalar().ToString();
                Cn.Close();


            }
            else if (Retorna == TipoEjecucion.DevuelveTable)
            {
                Tabla.Clear();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(Tabla);
            }
            else if (Retorna == TipoEjecucion.DevuelveDataSet)
            {
                DsTablas.Clear();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(DsTablas);
            }
            EsError = false;
        }
        catch (Exception ex)
        {
            EsError = true;
            Resultado = ex.Message;

        }
        finally
        {
            if (Cn.State == ConnectionState.Open)
                Cn.Close();
            cmd.Parameters.Clear();
        }
    }

    #region "   Transacciones   "
    public void IniciarTransaccion()
    {
        Cn.Open();
        Trans = Cn.BeginTransaction();
        cmd.Transaction = Trans;
    }

    public void ConfirmarTransaccion()
    {
        Trans.Commit();
        Cn.Close();
    }

    public void CancelarTransaccion()
    {
        if (Cn.State == ConnectionState.Open)
        {
            Trans.Rollback();
            Cn.Close();
        }
        cmd.Parameters.Clear();
    }

    public void EjecutarTransaccion(string procedimiento, TipoEjecucion Retorna = TipoEjecucion.DevuelveString, CommandType TipoComando = CommandType.StoredProcedure)
    {
        try
        {
            cmd.CommandType = TipoComando;
            cmd.CommandText = procedimiento;

            if (Retorna == TipoEjecucion.NoDevuelveNada)
            {
                Resultado = cmd.ExecuteNonQuery().ToString();

            }
            else if (Retorna == TipoEjecucion.DevuelveString)
            {
                Resultado = cmd.ExecuteScalar().ToString();

            }
            else if (Retorna == TipoEjecucion.DevuelveTable)
            {
                Tabla.Clear();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(Tabla);
            }
            else if (Retorna == TipoEjecucion.DevuelveDataSet)
            {
                DsTablas.Clear();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(DsTablas);
            }

            cmd.Parameters.Clear();
            EsError = false;

        }
        catch (Exception ex)
        {
            EsError = true;
            Resultado = ex.Message;
            CancelarTransaccion();
        }

    }
    #endregion

}
