﻿using BCPDominio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BCPOutsourcing
{
    public partial class frmUsuario : Form
    {
        public int idusuario { get; set; }
        public frmUsuario()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
        void CargarEmpresa()
        {
            BaseDato b = new BaseDato();
            b.Ejecutar("select * FROM empresa", BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            for (int i = 0; i < b.Tabla.Rows.Count; i++)
            {
                comboBox1.ValueMember = "idempresa";
                comboBox1.DisplayMember = "nombre";
                comboBox1.DataSource = b.Tabla;
            }
        }
        void CargarArea()
        {
            BaseDato b = new BaseDato();
            b.Ejecutar("select * FROM area", BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            for (int i = 0; i < b.Tabla.Rows.Count; i++)
            {
                comboBox2.ValueMember = "idarea";
                comboBox2.DisplayMember = "nombre";
                comboBox2.DataSource = b.Tabla;
            }
        }
        void CargarCargo()
        {
            BaseDato b = new BaseDato();
            b.Ejecutar("select * FROM cargo", BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            for (int i = 0; i < b.Tabla.Rows.Count; i++)
            {
                comboBox3.ValueMember = "idcargo";
                comboBox3.DisplayMember = "nombre";
                comboBox3.DataSource = b.Tabla;
            }
        }

        public void CargaUsuario() {
            BaseDato b = new BaseDato();
            b.AgregarParametro("@idusuario", idusuario);
            b.Ejecutar("select * from usuario where idusuario=@idusuario ", BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            if (b.Tabla.Rows.Count == 1) {
                textBox1.Text = b.Tabla.Rows[0]["usuario"].ToString();
                textBox2.Text = b.Tabla.Rows[0]["clave"].ToString();
                textBox3.Text = b.Tabla.Rows[0]["nombre"].ToString();
                textBox4.Text = b.Tabla.Rows[0]["apellidopaterno"].ToString();
                textBox5.Text = b.Tabla.Rows[0]["apellidomaterno"].ToString();
                dateTimePicker1.Value = DateTime.Parse(b.Tabla.Rows[0]["fechanacimiento"].ToString());
                
                comboBox1.SelectedValue = b.Tabla.Rows[0]["idempresa"].ToString();
                comboBox2.SelectedValue = b.Tabla.Rows[0]["idarea"].ToString();
                comboBox3.SelectedValue = b.Tabla.Rows[0]["idcargo"].ToString();
                comboBox4.Text = b.Tabla.Rows[0]["estado"].ToString() == "A" ? "Activo" : "Inactivo";

            }
        }

        void CargarUsuarioList()
        {
            try
            {
                foreach (Form frm in Application.OpenForms)
                {
                    if (frm.Name == "frmUsuarioList")
                    {
                        ((frmUsuarioList)frm).CargarUsuarios();
                        return;
                    }
                }
            }
            catch { }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            BaseDato b = new BaseDato();
            b.AgregarParametro("@usuario", textBox1.Text);
            b.AgregarParametro("@clave", textBox2.Text);
            b.AgregarParametro("@nombre", textBox3.Text);
            b.AgregarParametro("@apellidopaterno", textBox4.Text);
            b.AgregarParametro("@apellidomaterno", textBox5.Text);
            b.AgregarParametro("@fechanacimiento", dateTimePicker1.Value);
            b.AgregarParametro("@idempresa", comboBox1.SelectedValue);
            b.AgregarParametro("@idarea", comboBox2.SelectedValue);
            b.AgregarParametro("@idcargo", comboBox3.SelectedValue);
            b.AgregarParametro("@estado", (comboBox4.Text == "Activo" ? "A" : "I"));
            if (idusuario == 0)
            {
                b.Ejecutar("OUT_Usuario_Insert");
            }
            else {
                b.AgregarParametro("@idusuario", idusuario);
                b.Ejecutar("OUT_Usuario_Update",BaseDato.TipoEjecucion.NoDevuelveNada);
            }
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            CargarUsuarioList();
            Close();
        }

        private void frmUsuario_Load(object sender, EventArgs e)
        {
            CargarEmpresa();
            CargarArea();
            CargarCargo();
        }
    }
}
