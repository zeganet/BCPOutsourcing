﻿using BCPDominio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace BCPOutsourcing
{
    public partial class frmPCR : Form
    {
        public int idrequerimiento { get; set; }
        public int idpcr { get; set; }
        public frmPCR()
        {
            InitializeComponent();
        }
        void GenerarArchivo(string adjunto)
        {
            try
            {
                string[] a = { "#" };
                string[] archivo = adjunto.Split(a, StringSplitOptions.None);

                byte[] contenido = Ext.ToHexBytes(archivo[1]);
                
                FileStream stream = new FileStream(Path.GetDirectoryName(Application.ExecutablePath) + "\\FilesDownload\\" + archivo[0], FileMode.Create, FileAccess.Write);
                BinaryWriter writer = new BinaryWriter(stream);

                for (int i = 0; i < contenido.Length; i++)
                {
                    writer.Write(contenido[i]);
                }

                writer.Close();
                stream.Close();

                linkLabel1.Visible = true;
                linkLabel1.Text = archivo[0];
            }
            catch (Exception)
            {
                
            }



            //linkLabel1.NavigateUrl = hlArchivo.ResolveUrl("~/FilesDownload/" + archivo[0]);
        }
        private void frmPCR_Load(object sender, EventArgs e)
        {
            try
            {

                Obtieneidpcr();

                PCR pcr = CargarPCR(idpcr);
                if (pcr == null) return;
                label17.Text = pcr.idpcr.ToString();
                textBox4.Text = pcr.requerimiento.usuariosolicitante.usuario;
                textBox3.Text = pcr.requerimiento.fechasolicitud.ToShortDateString();
                textBox5.Text = pcr.requerimiento.usuarioGSTI.usuario;
                textBox7.Text = pcr.requerimiento.validaGSTI.ToShortDateString();
                textBox6.Text = pcr.requerimiento.usuarioOutsourcing.usuario;
                textBox8.Text = pcr.requerimiento.validaOutsourcing.ToShortDateString();
                textBox1.Text = pcr.requerimiento.motivo;
                textBox2.Text = pcr.requerimiento.detalle;
                textBox10.Text = pcr.detalle;
                textBox9.Text = pcr.descripcion;
                textBox11.Text = pcr.usuarioIBM.usuario;
                groupBox2.Text = "Datos del PCR Versión: " + pcr.version.ToString();
                GenerarArchivo(pcr.adjuntotecnico);
                CargarValidador();
                CargarValidadorPCR();
                Cotizacion cotizacion = CargarCotizacion(idpcr);
                if (cotizacion != null) {
                    textBox12.Text = cotizacion.monto.ToString();
                    textBox13.Text = cotizacion.descripcion.ToString();
                    textBox14.Text = cotizacion.fechacotizacion.ToShortDateString();
                }


                if (((frmParent)Parent.Parent).Usuario.cargo.idcargo != 3)
                {
                    button2.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void Obtieneidpcr()
        {
            BaseDato b = new BaseDato();

            string sql = "SELECT * FROM [dbo].[PCR] [p] WHERE [p].[activo]='A' AND [idestado] in (1,2) and idrequerimiento=@idrequerimiento";
            b.AgregarParametro("@idrequerimiento", idrequerimiento);
            b.Ejecutar(sql, BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado);}
            if (b.Tabla.Rows.Count == 1)
            {
                idpcr =  int.Parse(b.Tabla.Rows[0]["idpcr"].ToString());
            }
        }

        Cotizacion CargarCotizacion(int codigo)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(String.Format("http://localhost:56141/Cotizaciones/pcr/{0}", codigo));
            req.Method = "GET";
            req.ContentType = "application/json";
            req.Headers.Add("Authorization", ((frmParent)MdiParent).Lg.Oauth_token);
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            if (MensajeRespuesta(res))
            {
                using (StreamReader r = new StreamReader(res.GetResponseStream()))
                {
                    string json = r.ReadToEnd();
                    Cotizacion items = JsonConvert.DeserializeObject<Cotizacion>(json);
                    return items;
                }
            }
            else return null;
        }

        PCR CargarPCR(int codigo)
        {
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(String.Format("http://localhost:56141/PCRs/{0}", codigo));
                req.Method = "GET";
                req.ContentType = "application/json";
                req.Headers.Add("Authorization", ((frmParent)MdiParent).Lg.Oauth_token);
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                if (MensajeRespuesta(res))
                {
                    using (StreamReader r = new StreamReader(res.GetResponseStream()))
                    {
                        string json = r.ReadToEnd();
                        PCR items = JsonConvert.DeserializeObject<PCR>(json);
                        return items;
                    }
                }
                else return null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

        }

        void CargarValidadorPCR()
        {
            BaseDato b = new BaseDato();
            b.AgregarParametro("@idpcr", idpcr);
            b.Ejecutar("select * FROM validacion where idpcr=@idpcr", BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            for (int i = 0; i < b.Tabla.Rows.Count; i++)
            {
                for (int ii = 0; ii < checkedListBox1.Items.Count; ii++) {

                    if (((ValidacionTipo)checkedListBox1.Items[ii]).idvalidaciontipo == int.Parse(b.Tabla.Rows[i]["idvalidaciontipo"].ToString())) {
                        if (int.Parse(b.Tabla.Rows[i]["valida"].ToString()) == 1) {
                            checkedListBox1.SetItemChecked(ii, true);
                        }
                    }
                }
            }
        }

        void CargarValidador() {
            BaseDato b = new BaseDato();
            b.Ejecutar("select * FROM validaciontipo", BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            for (int i = 0; i < b.Tabla.Rows.Count; i++)
            {
                ValidacionTipo v = new ValidacionTipo()
                {
                    idvalidaciontipo = int.Parse(b.Tabla.Rows[i]["idvalidaciontipo"].ToString()),
                    nombre = b.Tabla.Rows[i]["nombre"].ToString(),
                };
                checkedListBox1.Items.Add(v);
            }
        }



        bool MensajeRespuesta(HttpWebResponse res)
        {
            switch (res.StatusCode)
            {
                case HttpStatusCode.Created:
                    return true;
                case HttpStatusCode.NoContent:
                    return true;
                case HttpStatusCode.OK:
                    return true;
                case HttpStatusCode.ExpectationFailed:
                    MessageBox.Show(res.StatusDescription);
                    return false;
                case HttpStatusCode.Forbidden:
                    MessageBox.Show(res.StatusDescription);
                    return false;
                case HttpStatusCode.NotImplemented:
                    MessageBox.Show(res.StatusDescription);
                    return false;
                default:
                    MessageBox.Show("Error no controlado");
                    return false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BaseDato b = new BaseDato();
            try
            {
                b.IniciarTransaccion();

                for (int i = 0; i < checkedListBox1.Items.Count; i++)
                {
                    b.AgregarParametro("@idvalidaciontipo", ((ValidacionTipo)checkedListBox1.Items[i]).idvalidaciontipo);
                    b.AgregarParametro("@idpcr", idpcr);
                    b.AgregarParametro("@valida", (checkedListBox1.GetItemChecked(i) ? "1" : "0"));
                    b.EjecutarTransaccion("OUT_Validacion_Insert",BaseDato.TipoEjecucion.NoDevuelveNada);
                    if (b.EsError) { MessageBox.Show(b.Resultado); b.CancelarTransaccion(); return; }
                }

                b.ConfirmarTransaccion();
                Close();
            }
            catch (Exception ex)
            {
                b.CancelarTransaccion();
                MessageBox.Show(ex.Message);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start(Path.GetDirectoryName(Application.ExecutablePath) + "\\FilesDownload\\" + linkLabel1.Text);
            }
            catch (Exception)
            {
            }
            
        }
    }
    public static class Ext
    {

        public static string ToHexString(this byte[] hex)
        {
            if (hex == null)
            {
                return null;
            }
            if (hex.Length == 0)
            {
                return string.Empty;
            }
            var s = new StringBuilder();
            foreach (byte b in hex)
            {
                s.Append(b.ToString("x2"));
            }
            return s.ToString();
        }

        public static byte[] ToHexBytes(this string hex)
        {
            if (hex == null)
            {
                return null;
            }
            if (hex.Length == 0)
            {
                return new byte[0];
            }
            int l = hex.Length / 2;
            var b = new byte[l];
            for (int i = 0; i < l; ++i)
            {
                b[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }
            return b;
        }
    }
}
