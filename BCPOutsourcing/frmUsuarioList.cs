﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BCPOutsourcing
{
    public partial class frmUsuarioList : Form
    {
        public frmUsuarioList()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmUsuario frm = new frmUsuario();
            frm.MdiParent = MdiParent;
            frm.idusuario = 0;
            frm.Show();
        }

        private void frmUsuarioList_Load(object sender, EventArgs e)
        {
            CargarUsuarios();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CargarUsuarios();
        }

        public void CargarUsuarios() {
            BaseDato b = new BaseDato();
            b.AgregarParametro("@usuario",textBox1.Text);
            b.Ejecutar("select 'Editar' editar,* from usuario where usuario like '%' + @usuario + '%' ", BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            dataGridView1.DataSource = b.Tabla;
            dataGridView1.Refresh();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                frmUsuario frm = new frmUsuario();
                frm.MdiParent = MdiParent;
                frm.Show();
                frm.idusuario = int.Parse(dataGridView1.Rows[e.RowIndex].Cells["idusuario"].Value.ToString());
                frm.CargaUsuario();
            }
        }
    }
}
