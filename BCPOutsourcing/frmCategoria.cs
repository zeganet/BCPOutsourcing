﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BCPOutsourcing
{
    public partial class frmCategoria : Form
    {
        public frmCategoria()
        {
            InitializeComponent();
        }
        public int idCategoria { get; set; }
        public string Nombre { get {
                return textBox1.Text;
            } set {
                textBox1.Text = value;
            }
        }

        private void frmCategoria_Load(object sender, EventArgs e)
        {
            
        }

        void Grabar() {
            BaseDato b = new BaseDato();
            b.AgregarParametro("@nombre", Nombre);
            b.Ejecutar("INSERT INTO[dbo].[Categoria] ([nombre]) VALUES(@nombre);",BaseDato.TipoEjecucion.NoDevuelveNada,CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
        }

        void Actualizar()
        {
            BaseDato b = new BaseDato();
            b.AgregarParametro("@idcategoria", idCategoria);
            b.AgregarParametro("@nombre", Nombre);

            string sql = "" +
                        "UPDATE[dbo].[Categoria] " +
                        "  SET [nombre] = @nombre " +
                        "WHERE[idcategoria] = @idcategoria; ";

            b.Ejecutar(sql, BaseDato.TipoEjecucion.NoDevuelveNada, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Grabar();
            CargaCategoria();
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Actualizar();
            CargaCategoria();
            Close();
        }

        void CargaCategoria()
        {
            try
            {
                foreach (Form frm in Application.OpenForms)
                {
                    if (frm.Name == "frmCategoriaList")
                    {
                        ((frmCategoriaList)frm).CargaCategoria();
                        return;
                    }
                }
            }
            catch{}
        }

    }
}
