﻿using System;
using BCPOutsourcing.Logins1;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BCPDominio;
using System.Messaging;

namespace BCPOutsourcing
{
    public partial class frmParent : Form
    {

        private login lg = null;
        public login Lg
        {
            get
            {
                return lg;
            }
            set {
                lg = value;
            }
        }

        private Usuario usuario = null;
        public Usuario Usuario
        {
            get
            {
                return usuario;
            }
            set
            {
                usuario = value;
            }
        }


        private int childFormNumber = 0;

        public frmParent()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Ventana " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
            Close();
        }

        private void fileMenu_Click(object sender, EventArgs e)
        {

        }

        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }

            System.Threading.Thread NuevoHilo = new System.Threading.Thread(new System.Threading.ThreadStart(RunLogin));
            Close();
            NuevoHilo.SetApartmentState(System.Threading.ApartmentState.STA);
            NuevoHilo.Start();

        }
        private void RunLogin()
        {
            frmLogin frm = new frmLogin();
            frm.ShowDialog();
        }

        private void frmParent_Load(object sender, EventArgs e)
        {
            lbUsuario.Text = Lg.Usuario;
            lbNombre.Text = Lg.Mensaje;
            lbHora.Text = Lg.Hora;
            CheckForIllegalCrossThreadCalls = false;
            ObtieneCargo();

            if (usuario == null) {
                MessageBox.Show("Problemas al intentar obtener el perfil del usuario");
                cerrarSesiónToolStripMenuItem_Click(sender, e);
                return;
            }

            switch (Usuario.cargo.idcargo)
            {
                case 1: //USUARIO
                    //
                    administToolStripMenuItem.Visible = false;
                    creaciónYGestiónDeUsuariosToolStripMenuItem.Visible = false;
                    mantenimientoDeCategoriaToolStripMenuItem.Visible = false;
                    mantenimientoDeServiciosToolStripMenuItem.Visible = false;
                    //
                    requerimientoDeServicioToolStripMenuItem.Visible = true;
                    validarRequerimientoDeServicioToolStripMenuItem.Visible = false;
                    //
                    estadoDelRequerimientoDeServicioToolStripMenuItem.Visible = true;
                    break;
                case 2: //GSTI
                    //
                    administToolStripMenuItem.Visible = false;
                    creaciónYGestiónDeUsuariosToolStripMenuItem.Visible = false;
                    mantenimientoDeCategoriaToolStripMenuItem.Visible = false;
                    mantenimientoDeServiciosToolStripMenuItem.Visible = false;
                    //
                    requerimientoDeServicioToolStripMenuItem.Visible = true;
                    validarRequerimientoDeServicioToolStripMenuItem.Visible = true;
                    //
                    consultasToolStripMenuItem.Visible = false;
                    estadoDelRequerimientoDeServicioToolStripMenuItem.Visible = false;
                    break;
                case 3: //Outsourcing
                    //
                    administToolStripMenuItem.Visible = true;
                    creaciónYGestiónDeUsuariosToolStripMenuItem.Visible = true;
                    mantenimientoDeCategoriaToolStripMenuItem.Visible = true;
                    mantenimientoDeServiciosToolStripMenuItem.Visible = true;
                    //
                    requerimientoDeServicioToolStripMenuItem.Visible = false;
                    validarRequerimientoDeServicioToolStripMenuItem.Visible = true;
                    //
                    consultasToolStripMenuItem.Visible = false;
                    estadoDelRequerimientoDeServicioToolStripMenuItem.Visible = false;
                    break;
                default:
                    break;
            }


        }
        void ObtieneCargo() {
            BaseDato b = new BaseDato();
            b.AgregarParametro("@token", Lg.Oauth_token);
            b.Ejecutar("select * from acceso a inner join usuario u on u.idusuario=a.idusuario where token=@token", BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            if (b.Tabla.Rows.Count == 1)
            {
                Usuario = new Usuario();
                Usuario.idusuario = int.Parse(b.Tabla.Rows[0]["idusuario"].ToString());
                Usuario.cargo = new Cargo() { idcargo = int.Parse(b.Tabla.Rows[0]["idcargo"].ToString()) };
            }
            else { MessageBox.Show("Problema de seguridad al detectar un token duplicado"); return; }
        }
        private void creaciónYGestiónDeUsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUsuarioList frm = new frmUsuarioList();
            frm.MdiParent = this;
            frm.Show();
        }

        private void reportesPorEstadosDelServicioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRptEstadoServicio frm = new frmRptEstadoServicio();
            frm.MdiParent = this;
            frm.Show();
        }

        private void reportePorServicioAgrupadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRptServicioAgrupo frm = new frmRptServicioAgrupo();
            frm.MdiParent = this;
            frm.Show();
        }

        private void requerimientoDeServicioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmServicioList frm = new frmServicioList();
            frm.MdiParent = this;
            frm.Show();
        }

        private void estadoDelRequerimientoDeServicioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRequerimientoValidarList frm = new frmRequerimientoValidarList();
            frm.MdiParent = this;
            frm.Show();
        }

        private void mantenimientoDeCategoriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCategoriaList frm = new frmCategoriaList();
            frm.MdiParent = this;
            frm.Show();
        }

        private void validarRequerimientoDeServicioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRequerimientoValidarList frm = new frmRequerimientoValidarList();
            frm.MdiParent = this;
            frm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ObtieneCargo();
        }

        private void mantenimientoDeServiciosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmServicioList frm = new frmServicioList();
            frm.MdiParent = this;
            frm.Show();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            string rutaColaIn = @".\private$\BCPMensaje";
            if (!MessageQueue.Exists(rutaColaIn))
                MessageQueue.Create(rutaColaIn);
            MessageQueue colaIn = new MessageQueue(rutaColaIn);
            colaIn.Formatter = new XmlMessageFormatter(new Type[] { typeof(PCR) });
            System.Messaging.Message mensajeIn = colaIn.Receive();
            PCR pcr = (PCR)mensajeIn.Body;
            //listBox1.FormattingEnabled = True
            if (Usuario.cargo.idcargo != 3)
            {
                timer1.Enabled = false;
                return;
            }
            listBox1.Items.Add(pcr);
            notifyIcon1.BalloonTipTitle = "Tiene un nuevo PCR generado";
            notifyIcon1.BalloonTipText = pcr.detalle;
            notifyIcon1.Icon = new Icon(SystemIcons.Application, 40, 40);
            notifyIcon1.Visible = true;
            notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
            notifyIcon1.ShowBalloonTip(10000);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Usuario.cargo.idcargo != 3) {
                timer1.Enabled = false;
                return;
            }
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync();
            }

        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            CargaPCR();
        }
        void CargaPCR() {
            try
            {
                PCR pcr = (PCR)listBox1.Items[0];
                frmPCR frm = new frmPCR();
                frm.idrequerimiento = pcr.requerimiento.idrequerimiento;
                frm.MdiParent = this;
                frm.Show();

                listBox1.Items.RemoveAt(0);
            }
            catch (Exception)
            {
                
            }

        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            CargaPCR();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            CargaPCR();
        }
    }
}
