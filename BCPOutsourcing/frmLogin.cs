﻿using BCPDominio;
using BCPOutsourcing.Logins1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel.Security;
using System.Text;
using System.Windows.Forms;

namespace BCPOutsourcing
{
    public partial class frmLogin : Form
    {
        LoginsClient client;
        login l;
        public frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            if (client == null) { client = new LoginsClient(); }
            client.ClientCredentials.UserName.UserName = "WSBCP";
            client.ClientCredentials.UserName.Password = "REVDNTI3QTUtREU5MC00QkVGLUJDQkEtRjI5RDREMTI2QjNFU3lzdGVtLkJ5dGVbXQ";
            client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
        }

        private void btIniciarSession_Click(object sender, EventArgs e)
        {

            l = client.ValidarSession(new Usuario() { usuario=tbUsuario.Text,clave=tbClave.Text,empresa=new Empresa() { idempresa=1 } });

            if (l.Success)
            {
                System.Threading.Thread NuevoHilo = new System.Threading.Thread(new System.Threading.ThreadStart(RunPrincipal));
                this.Close();
                NuevoHilo.SetApartmentState(System.Threading.ApartmentState.STA);
                NuevoHilo.Start();
            }
            else {
                MessageBox.Show(l.Mensaje, Application.ProductName, buttons: MessageBoxButtons.OK, icon: MessageBoxIcon.Warning); return;
            }

        }
        private void RunPrincipal()
        {
            frmParent frm = new frmParent();
            frm.Lg = l;
            frm.ShowDialog();
        }
    }
}
