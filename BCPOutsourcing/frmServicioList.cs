﻿using BCPDominio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace BCPOutsourcing
{
    public partial class frmServicioList : Form
    {


        public void CargaServicio(string detalle="")
        {
            BaseDato b = new BaseDato();
            string sql;
            //sql += ((frmParent)Parent.Parent).Usuario.cargo.idcargo != 1 ? " 'Editar' Editar, " : "";'
            sql = "select ";
            sql +=" 'Editar' Editar,p.*,s.* " +
                "from Servicio s inner join " +
                "pcr p on s.idservicio = p.idpcr ";

            if (detalle != "") {
                sql += "where p.detalle like '%'+@detalle+'%' ";
                b.AgregarParametro("@detalle", detalle);
            }

            b.Ejecutar(sql, BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            dataGridView1.DataSource = b.Tabla;
            dataGridView1.Refresh();
        }

        void CargaCategoria()
        {
            BaseDato b = new BaseDato();
            string sql;
            sql = "select * from categoria ";
            b.Ejecutar(sql, BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            comboBox1.DataSource = b.Tabla;
            comboBox1.DisplayMember = "nombre";
            comboBox1.ValueMember = "idcategoria";
            comboBox1.Refresh();
        }

        public frmServicioList()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmRequerimientoServicio frm = new frmRequerimientoServicio();
            frm.isLectura = false;
            frm.MdiParent = this.MdiParent;
            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CargaServicio(textBox1.Text);
        }

        void CargarGrillaRequerimiento()
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://localhost:56141/Requerimientos/");
            req.Method = "GET";
            req.ContentType = "application/json";
            req.Headers.Add("Authorization", ((frmParent)this.Parent.Parent).Lg.Oauth_token);
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            using (StreamReader r = new StreamReader(res.GetResponseStream()))
            {
                string json = r.ReadToEnd();
                List<Requerimiento> items = JsonConvert.DeserializeObject<List<Requerimiento>>(json);
                dataGridView1.DataSource = items;
                dataGridView1.Refresh();
            }
        }

        private void frmRequerimientoServicioList_Load(object sender, EventArgs e)
        {
            CargaServicio();
            CargaCategoria();
            if (((frmParent)Parent.Parent).Usuario.cargo.idcargo != 1)
            {
                button1.Visible = false;
                button3.Visible = true;
                dataGridView1.Columns[0].Visible = false;//true
            }
            else {
                button3.Visible = false;
                dataGridView1.Columns[0].Visible = false;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                frmPCRToServicio frm = new frmPCRToServicio();
                frm.MdiParent = MdiParent;
                frm.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmPCRToServicio frm = new frmPCRToServicio();
            frm.MdiParent = MdiParent;
            frm.Show();
        }
    }
}
