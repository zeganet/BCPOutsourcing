﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BCPOutsourcing
{
    public partial class frmRequerimientoServicio : Form
    {
        public bool isLectura { get; set; }
        public int idrequerimiento { get; set; }

        public frmRequerimientoServicio()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            BaseDato b = new BaseDato();
            b.AgregarParametro("@idusuariosolicitante", ((frmParent)Parent.Parent).Lg.Idusuario);
            b.AgregarParametro("@fechasolicitud", DateTime.Now);
            b.AgregarParametro("@motivo", textBox1.Text);
            b.AgregarParametro("@detalle", textBox2.Text);
            //b.AgregarParametro("@idusuarioGSTI", null);
            b.AgregarParametro("@estado", "A");
            b.Ejecutar("OUT_Requerimiento_Insert");
            if (b.EsError) { MessageBox.Show(b.Resultado);return; }

            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmRequerimientoServicio_Load(object sender, EventArgs e)
        {
            switch (((frmParent)Parent.Parent).Usuario.cargo.idcargo)
            {
                case 1: // USUARIO
                    button2.Visible = true;
                    button3.Visible = false;
                    button4.Visible = false;
                    button5.Visible = false;
                    if (!isLectura)
                    {
                        textBox1.Text = ""; textBox1.ReadOnly = false;
                        textBox2.Text = ""; textBox2.ReadOnly = false;
                        textBox3.Text = DateTime.Now.ToShortDateString();
                        textBox4.Text = ((frmParent)Parent.Parent).Lg.Mensaje;
                        textBox5.Text = "Pendiente por validar";
                        textBox6.Text = "Pendiente por validar";
                    }
                    break;
                case 2: // GSTI
                    button2.Visible = false;
                    button3.Visible = true;
                    button4.Visible = true;
                    button5.Visible = false;
                    textBox1.ReadOnly = true;
                    textBox2.ReadOnly = true;
                    break;
                case 3: // Outsourcing
                    button2.Visible = false;
                    button3.Visible = true;
                    button4.Visible = true;
                    button5.Visible = true;
                    textBox1.ReadOnly = false;
                    textBox2.ReadOnly = false;
                    break;
            }
            if (isLectura) {
                button2.Visible = false;
            }
        }

        public void Bloquear() {
            button3.Visible = false;
            button4.Visible = false;
       }

        public void CargaRequerimiento() {

            BaseDato b = new BaseDato();
            string sql = "select * FROM requerimiento r " +
            "inner JOIN (SELECT idusuario,[usuario] 'solicitante' FROM [dbo].[Usuario]) u ON [u].[idusuario] = [r].[idusuariosolicitante]  " +
            "left JOIN (SELECT idusuario,[usuario] 'gsti' FROM [dbo].[Usuario]) u1 ON [u1].[idusuario] = [r].[idusuarioGSTI]  " +
            "left JOIN (SELECT idusuario,[usuario] 'outsourcing' FROM [dbo].[Usuario]) u2 ON [u2].[idusuario] = [r].[idusuarioOutsourcing]  " +
            "where idrequerimiento=@idrequerimiento ";

            b.AgregarParametro("@idrequerimiento", idrequerimiento);
            b.Ejecutar(sql, BaseDato.TipoEjecucion.DevuelveTable,CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            if (b.Tabla.Rows.Count > 0) {
                textBox1.Text = b.Tabla.Rows[0]["motivo"].ToString();
                textBox2.Text = b.Tabla.Rows[0]["detalle"].ToString();
                textBox3.Text = b.Tabla.Rows[0]["fechasolicitud"].ToString();
                textBox4.Text = b.Tabla.Rows[0]["solicitante"].ToString();
                textBox5.Text = (b.Tabla.Rows[0]["gsti"].ToString() != "" ? b.Tabla.Rows[0]["gsti"].ToString() : "Pendiente por validar");
                textBox6.Text = (b.Tabla.Rows[0]["outsourcing"].ToString() != "" ? b.Tabla.Rows[0]["outsourcing"].ToString() : "Pendiente por validar");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Aprobar
            if (MessageBox.Show("Desea Aprobar el requerimiento del usuario?", "BCP", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                BaseDato b = new BaseDato();
                if (((frmParent)Parent.Parent).Usuario.cargo.idcargo == 2)
                {//GSTI
                    b.AgregarParametro("@idrequerimiento", idrequerimiento);
                    b.AgregarParametro("@idusuarioGSTI", ((frmParent)Parent.Parent).Lg.Idusuario);
                    b.Ejecutar("update requerimiento set estado='A',idusuarioGSTI=@idusuarioGSTI where idrequerimiento=@idrequerimiento", BaseDato.TipoEjecucion.NoDevuelveNada, CommandType.Text);
                    if (b.EsError) { MessageBox.Show(b.Resultado); return; }
                }
                else if (((frmParent)Parent.Parent).Usuario.cargo.idcargo == 3)
                {//Outsorcing
                    b.AgregarParametro("@idrequerimiento", idrequerimiento);
                    b.AgregarParametro("@idusuarioOutsourcing", ((frmParent)Parent.Parent).Lg.Idusuario);
                    b.Ejecutar("update requerimiento set estado='AO',idusuarioOutsourcing=@idusuarioOutsourcing where idrequerimiento=@idrequerimiento", BaseDato.TipoEjecucion.NoDevuelveNada, CommandType.Text);
                    if (b.EsError) { MessageBox.Show(b.Resultado); return; }
                }
                CargaRequerimientoList();
                Close();
            }
        }
        void CargaRequerimientoList()
        {
            try
            {
                foreach (Form frm in Application.OpenForms)
                {
                    if (frm.Name == "frmRequerimientoValidarList")
                    {
                        ((frmRequerimientoValidarList)frm).CargaRequerimientoList();
                        return;
                    }
                }
            }
            catch { }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            //Rechazar
            if (MessageBox.Show("Desea rechazar el requerimiento del usuario?","BCP",MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes) {
                BaseDato b = new BaseDato();
                if (((frmParent)Parent.Parent).Usuario.cargo.idcargo == 2) {//GSTI
                    b.AgregarParametro("@idrequerimiento", idrequerimiento);
                    b.AgregarParametro("@idusuarioGSTI", ((frmParent)Parent.Parent).Lg.Idusuario);
                    b.Ejecutar("update requerimiento set estado='RG',idusuarioGSTI=@idusuarioGSTI where idrequerimiento=@idrequerimiento", BaseDato.TipoEjecucion.NoDevuelveNada, CommandType.Text);
                    if (b.EsError) { MessageBox.Show(b.Resultado); return; }
                }
                else if (((frmParent)Parent.Parent).Usuario.cargo.idcargo == 3)
                {//Outsorcing
                    b.AgregarParametro("@idrequerimiento", idrequerimiento);
                    b.AgregarParametro("@idusuarioOutsourcing", ((frmParent)Parent.Parent).Lg.Idusuario);
                    b.Ejecutar("update requerimiento set estado='RO',idusuarioOutsourcing=@idusuarioOutsourcing where idrequerimiento=@idrequerimiento", BaseDato.TipoEjecucion.NoDevuelveNada, CommandType.Text);
                    if (b.EsError) { MessageBox.Show(b.Resultado); return; }
                }
                CargaRequerimientoList();
                Close();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //Actualizar Requerimiento
            BaseDato b = new BaseDato();
            b.AgregarParametro("@idrequerimiento", idrequerimiento);
            b.AgregarParametro("@motivo", textBox1.Text);
            b.AgregarParametro("@detalle", textBox2.Text);
            b.Ejecutar("OUT_Requerimiento_Update",BaseDato.TipoEjecucion.NoDevuelveNada);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }

            Close();
        }
    }
}
