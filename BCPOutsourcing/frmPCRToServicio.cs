﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BCPOutsourcing
{
    public partial class frmPCRToServicio : Form
    {
        public int idservicio { get; set; }
        public frmPCRToServicio()
        {
            InitializeComponent();
        }

        private void frmPCRToServicio_Load(object sender, EventArgs e)
        {
            CargarCategoria();
            textBox1.Text = "";
            textBox2.Text = "";
        }
        void CargarCategoria()
        {
            BaseDato b = new BaseDato();
            b.Ejecutar("select * FROM categoria", BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            for (int i = 0; i < b.Tabla.Rows.Count; i++)
            {
                comboBox1.ValueMember = "idcategoria";
                comboBox1.DisplayMember = "nombre";
                comboBox1.DataSource = b.Tabla;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BaseDato b = new BaseDato();
            b.AgregarParametro("@idpcr", textBox1.Text);
            b.Ejecutar("select * from pcr a left join servicio b on a.idpcr=b.idservicio where idpcr=@idpcr and a.activo='A'", BaseDato.TipoEjecucion.DevuelveTable, CommandType.Text);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            if (b.Tabla.Rows.Count == 1)
            {
                if (b.Tabla.Rows[0]["idservicio"].ToString() != "") {
                    MessageBox.Show("El servicio con el identificador ingresado ya esta creado");
                    return;
                }
                idservicio = int.Parse(textBox1.Text);
                textBox3.Text = b.Tabla.Rows[0]["detalle"].ToString();
            }
            else {
                MessageBox.Show("El identificador ingresado no esta disponible");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BaseDato b = new BaseDato();
            b.AgregarParametro("@idservicio", idservicio);
            b.AgregarParametro("@idcategoria", comboBox1.SelectedValue);
            b.AgregarParametro("@nombre", textBox2.Text);
            b.AgregarParametro("@idusuarioregistra", ((frmParent)Parent.Parent).Usuario.idusuario);
            b.Ejecutar("OUT_Servicio_Insert", BaseDato.TipoEjecucion.DevuelveString, CommandType.StoredProcedure);
            if (b.EsError) { MessageBox.Show(b.Resultado); return; }
            CargaServicioList();
            Close();
        }


        void CargaServicioList()
        {
            try
            {
                foreach (Form frm in Application.OpenForms)
                {
                    if (frm.Name == "frmServicioList")
                    {
                        ((frmServicioList)frm).CargaServicio();
                        return;
                    }
                }
            }
            catch { }
        }

    }
}
