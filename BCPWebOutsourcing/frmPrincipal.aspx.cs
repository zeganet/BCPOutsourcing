﻿using BCPDominio;
using BCPWebOutsourcing.Logins1;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace BCPWebOutsourcing
{
    public partial class frmPrincipal : System.Web.UI.Page
    {
        login l;
        protected void Page_Load(object sender, EventArgs e)
        {
            lbMensaje.Text = "";
            l = (login)Session["login"];
            if (!IsPostBack)
            {
                
                if (l != null)
                {
                    try
                    {
                        lbUsuario.Text = l.Usuario;
                        lbApellidosNombre.Text = l.Mensaje;
                        lbHoraAcceso.Text = l.Hora;
                        CargarGrillaRequerimiento();
                        CargarGrillaPCR();
                        CargarEstadoPCR();
                    }
                    catch(Exception ex)
                    {
                        if (ex.Message.Contains("403")) 
                            lbMensaje.Text = Mensajes.Ambar("No autorizado a consumir el servicio <a href=\"frmLogin.aspx\">Inicie sesión</a>");
                        else lbMensaje.Text =Mensajes.Ambar(ex.Message);
                    }
                }
            }
        }

        protected void btSessionCerrar_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Server.Transfer("frmLogin.aspx");
        }

        protected void imgCotizar_Click(object sender, ImageClickEventArgs e)
        {
            //mpeCotizar.show();
        }

        protected void gvRequerimientoPCR_OnRowCommand(Object sender, GridViewCommandEventArgs e)
        {
            try
            {
                gvRequerimientoPCR.SelectedIndex = int.Parse(e.CommandArgument.ToString());
                if (e.CommandName == "CrearPCR")
                {
                    Requerimiento requerimiento = CargarRequerimiento(int.Parse(gvRequerimientoPCR.SelectedValue.ToString()));
                    lbFechaSolicitud.Text = requerimiento.fechasolicitud.ToShortDateString();
                    lbMotivo.Text = requerimiento.motivo;
                    lbDetalleRequerimiento.Text = requerimiento.detalle;
                    lbSolicitante.Text = requerimiento.usuariosolicitante.nombre + " " + requerimiento.usuariosolicitante.apellidopaterno;
                    lbResponsableGSTI.Text = requerimiento.usuarioGSTI.nombre + " " + requerimiento.usuarioGSTI.apellidopaterno;
                    lbResponsableIBM.Text = ((login)Session["login"]).Mensaje;

                    lbVersion.Text = "1";
                    tbPCRResumen.Text = "";
                    tbPCRDescripcion.Text = "";
                    tbArchivo.Text = "";
                    lbPCRTitulo.Text = "Crear PCR";

                    FileArchivoTecnico.Visible = true;
                    hlArchivo.Visible = false;

                    tbPCRDescripcion.Enabled = true;
                    tbPCRResumen.Enabled = true;
                    //FileArchivoTecnico.Enabled = true;

                    btRequerimientoGrabar.Visible = true;
                    btPCRActualizar.Visible = false;

                    mpeRequerimiento.Show();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("403"))
                    lbMensaje.Text = Mensajes.Ambar("No autorizado a consumir el servicio <a href=\"frmLogin.aspx\">Inicie sesión</a>");
                else lbMensaje.Text = Mensajes.Rojo(ex.Message);
            }


        }

        protected void gvPCR_OnRowCommand(Object sender, GridViewCommandEventArgs e)
        {
            try
            {
                gvPCR.SelectedIndex = int.Parse(e.CommandArgument.ToString());
                if (e.CommandName == "ElimnarPCR")
                {
                    PCR pcr = new PCR();
                    pcr.idpcr = int.Parse(gvPCR.SelectedValue.ToString());
                    HttpWebResponse res = EliminarPCR(pcr);
                    if (MensajeRespuesta(res)) {
                        CargarGrillaPCR();
                        CargarGrillaRequerimiento();
                    }
                }
                else if (e.CommandName == "EditarPCR")
                {
                    PCR pcr = CargarPCR(int.Parse(gvPCR.SelectedValue.ToString()));
                    Requerimiento requerimiento = CargarRequerimiento(pcr.requerimiento.idrequerimiento);
                    lbFechaSolicitud.Text = requerimiento.fechasolicitud.ToShortDateString();
                    lbMotivo.Text = requerimiento.motivo;
                    lbDetalleRequerimiento.Text = requerimiento.detalle;
                    lbSolicitante.Text = requerimiento.usuariosolicitante.nombre + " " + requerimiento.usuariosolicitante.apellidopaterno;
                    lbResponsableGSTI.Text = requerimiento.usuarioGSTI.nombre + " " + requerimiento.usuarioGSTI.apellidopaterno;
                    lbResponsableIBM.Text = ((login)Session["login"]).Mensaje;
                    tbPCRDescripcion.Text = pcr.descripcion;
                    tbPCRResumen.Text = pcr.detalle;
                    lbVersion.Text = pcr.version.ToString();

                    tbArchivo.Text = "";

                    FileArchivoTecnico.Visible = true;
                    hlArchivo.Visible = true;

                    GenerarArchivo(pcr.adjuntotecnico);

                    tbPCRDescripcion.Enabled = true;
                    tbPCRResumen.Enabled = true;
                    //FileArchivoTecnico.Enabled = true;

                    lbPCRTitulo.Text = "Editar PCR";
                    btRequerimientoGrabar.Visible = false;
                    btPCRActualizar.Visible = true;


                    mpeRequerimiento.Show();
                }
                else if (e.CommandName == "VerPCR")
                {
                    PCR pcr = CargarPCR(int.Parse(gvPCR.SelectedValue.ToString()));
                    Requerimiento requerimiento = CargarRequerimiento(pcr.requerimiento.idrequerimiento);
                    lbFechaSolicitud.Text = requerimiento.fechasolicitud.ToShortDateString();
                    lbMotivo.Text = requerimiento.motivo;
                    lbDetalleRequerimiento.Text = requerimiento.detalle;
                    lbSolicitante.Text = requerimiento.usuariosolicitante.nombre + " " + requerimiento.usuariosolicitante.apellidopaterno;
                    lbResponsableGSTI.Text = requerimiento.usuarioGSTI.nombre + " " + requerimiento.usuarioGSTI.apellidopaterno;
                    lbResponsableIBM.Text = ((login)Session["login"]).Mensaje;
                    tbPCRDescripcion.Text = pcr.descripcion;
                    tbPCRResumen.Text = pcr.detalle;
                    lbVersion.Text = pcr.version.ToString();

                    GenerarArchivo(pcr.adjuntotecnico);


                    FileArchivoTecnico.Visible = false;

                    tbPCRDescripcion.Enabled = false;
                    tbPCRResumen.Enabled = false;
                    //FileArchivoTecnico.Enabled = false;

                    lbPCRTitulo.Text = "Ver PCR";
                    btRequerimientoGrabar.Visible = false;
                    btPCRActualizar.Visible = false;


                    mpeRequerimiento.Show();
                }
                else if (e.CommandName == "CotizarPCR")
                {
                    Cotizacion cotizacion = CargarCotizacion(int.Parse(gvPCR.SelectedValue.ToString()));
                    if (cotizacion == null)
                    {
                        tbCotizarMonto.Text = "";
                        tbCotizarDescripcion.Text = "";
                    }
                    else {
                        tbCotizarMonto.Text = cotizacion.monto.ToString();
                        tbCotizarDescripcion.Text = cotizacion.descripcion;
                    }
                    mpeCotizar.Show();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("403"))
                    lbMensaje.Text = Mensajes.Ambar("No autorizado a consumir el servicio <a href=\"frmLogin.aspx\">Inicie sesión</a>");
                else lbMensaje.Text = Mensajes.Rojo(ex.Message);
            }
        }

        void GenerarArchivo(string adjunto) {
            string[] a = { "#" };
            string[] archivo = adjunto.Split(a, StringSplitOptions.None);

            byte[] contenido = Ext.ToHexBytes(archivo[1]);
            //byte[] contenido = Encoding.UTF8.GetBytes(archivo[1]);
            //byte[] contenido = Convert.FromBase64String(archivo[1]);

            FileStream stream = new FileStream(Server.MapPath("~") + "\\FilesDownload\\" + archivo[0], FileMode.Create, FileAccess.Write);
            BinaryWriter writer = new BinaryWriter(stream);

            for (int i = 0; i < contenido.Length; i++)
            {
                // números son guardados en formáto UTF-8 format (4 bytes)
                writer.Write(contenido[i]);
            }

            writer.Close();
            stream.Close();

            hlArchivo.Visible = true;
            hlArchivo.Text = archivo[0];
            hlArchivo.Target = "_blank";
            hlArchivo.NavigateUrl = hlArchivo.ResolveUrl("~/FilesDownload/" + archivo[0]);
        }

        protected void btRequerimientoGrabar_Click(object sender, EventArgs e)
        {
            try
            {

                if (tbArchivo.Text=="") {
                    throw new ArgumentException("Debe seleccionar el documento tecnico");
                }
                if (tbPCRResumen.Text == "")
                {
                    throw new ArgumentException("Debe ingresar el detalle del PCR");
                }

                if (tbPCRDescripcion.Text == "")
                {
                    throw new ArgumentException("Debe ingresar la descripcion del PCR");
                }

                PCR pcr = new PCR();

                pcr.requerimiento = new Requerimiento() { idrequerimiento = int.Parse(gvRequerimientoPCR.SelectedValue.ToString()) };
                pcr.estado = new Estado() { idestado = 1 };
                pcr.version = int.Parse(lbVersion.Text) ;
                pcr.detalle = tbPCRResumen.Text;
                pcr.descripcion = tbPCRDescripcion.Text;
                pcr.usuarioIBM = new Usuario() { idusuario = ((login)Session["login"]).Idusuario };
                pcr.usuarioOutsourcing = new Usuario() { idusuario = ((login)Session["login"]).Idusuario };

                FileStream stream = new FileStream(Server.MapPath("~") + "\\FilesUpload\\" + tbArchivo.Text, FileMode.Open, FileAccess.Read);
                BinaryReader reader = new BinaryReader(stream);
                pcr.adjuntotecnico = tbArchivo.Text + "#" + Ext.ToHexString(ReadFully(reader.BaseStream));
                reader.Close();
                stream.Close();
                //****

                pcr.activo = "A";
                pcr.fecharegistro = DateTime.Now;
                HttpWebResponse res = GrabarPCR(pcr);
                if (MensajeRespuesta(res)) {
                    mpeRequerimiento.Hide();
                    CargarGrillaPCR();
                    CargarGrillaRequerimiento();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("403"))
                    lbMensaje.Text = Mensajes.Ambar("No autorizado a consumir el servicio <a href=\"frmLogin.aspx\">Inicie sesión</a>");
                else lbMensaje.Text = Mensajes.Rojo(ex.Message);
            }

        }

        protected void btBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarGrillaPCR(ddlEstado.SelectedValue);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("403"))
                    lbMensaje.Text = Mensajes.Ambar("No autorizado a consumir el servicio <a href=\"frmLogin.aspx\">Inicie sesión</a>");
                else lbMensaje.Text = Mensajes.Rojo(ex.Message);
            }

        }

        protected void btPCRActualizar_Click(object sender, EventArgs e)
        {
            try
            {



                if (tbArchivo.Text == "")
                {
                    throw new ArgumentException("Debe seleccionar el documento tecnico");
                }
                if (tbPCRResumen.Text == "")
                {
                    throw new ArgumentException("Debe ingresar el detalle del PCR");
                }

                if (tbPCRDescripcion.Text == "")
                {
                    throw new ArgumentException("Debe ingresar la descripcion del PCR");
                }

                PCR pcr = CargarPCR(int.Parse(gvPCR.SelectedValue.ToString()));
                //PCR pcr = new PCR();

                //pcr.requerimiento = new Requerimiento() { idrequerimiento = int.Parse(gvRequerimientoPCR.SelectedValue.ToString()) };
                pcr.estado = new Estado() { idestado = 1 };
                pcr.version = int.Parse(lbVersion.Text) + 1;
                pcr.detalle = tbPCRResumen.Text;
                pcr.descripcion = tbPCRDescripcion.Text;
                pcr.usuarioIBM = new Usuario() { idusuario = ((login)Session["login"]).Idusuario };
                pcr.usuarioOutsourcing = new Usuario() { idusuario = ((login)Session["login"]).Idusuario };
                FileStream stream = new FileStream(Server.MapPath("~") + "\\FilesUpload\\" + tbArchivo.Text, FileMode.Open, FileAccess.Read);
                BinaryReader reader = new BinaryReader(stream);
                pcr.adjuntotecnico = tbArchivo.Text + "#" + Convert.ToBase64String(ReadFully(reader.BaseStream));
                reader.Close();
                stream.Close();
                pcr.activo = "A";
                pcr.fecharegistro = DateTime.Now;
                HttpWebResponse res = GrabarPCR(pcr);
                if (MensajeRespuesta(res)) {
                    mpeRequerimiento.Hide();
                    CargarGrillaPCR();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("403"))
                    lbMensaje.Text = Mensajes.Ambar("No autorizado a consumir el servicio <a href=\"frmLogin.aspx\">Inicie sesión</a>");
                else lbMensaje.Text = Mensajes.Ambar(ex.Message);
            }
        }

        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }


        //string CargarREST(string linkREST) {
        //    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(linkREST);
        //    req.Method = "GET";
        //    req.ContentType = "application/json";
        //    HttpWebResponse res = (HttpWebResponse)req.GetResponse();
        //    switch (res.StatusCode)
        //    {
        //        case HttpStatusCode.Created:
        //            //mpeRequerimiento.Hide();
        //            //CargarGrillaPCR();
        //            break;
        //        case HttpStatusCode.ExpectationFailed:
        //            lbMensaje.Text = Mensajes.Ambar(res.StatusDescription);
        //            break;
        //        default:
        //            lbMensaje.Text = Mensajes.Rojo("Error no controlado");
        //            break;
        //    }
        //    return new StreamReader(res.GetResponseStream()).ReadToEnd();
        //}


        void CargarEstadoPCR()
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://localhost:56141/Estados/");
            req.Method = "GET";
            req.ContentType = "application/json";
            req.Headers.Add("Authorization", l.Oauth_token);
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            if (MensajeRespuesta(res))
            {
                using (StreamReader r = new StreamReader(res.GetResponseStream()))
                {
                    string json = r.ReadToEnd();
                    List<Estado> items = JsonConvert.DeserializeObject<List<Estado>>(json);
                    ddlEstado.DataSource = items;
                    ddlEstado.DataTextField = "nombre";
                    ddlEstado.DataValueField = "idestado";
                    ddlEstado.DataBind();
                }
            }
        }

        PCR CargarPCR(int codigo)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(String.Format("http://localhost:56141/PCRs/{0}", codigo));
            req.Method = "GET";
            req.ContentType = "application/json";
            req.Headers.Add("Authorization", l.Oauth_token);
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            if (MensajeRespuesta(res))
            {
                using (StreamReader r = new StreamReader(res.GetResponseStream()))
                {
                    string json = r.ReadToEnd();
                    PCR items = JsonConvert.DeserializeObject<PCR>(json);
                    return items;
                }
            }
            else return null;
        }

        Cotizacion CargarCotizacion(int codigo)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(String.Format("http://localhost:56141/Cotizaciones/pcr/{0}", codigo));
            req.Method = "GET";
            req.ContentType = "application/json";
            req.Headers.Add("Authorization", l.Oauth_token);
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            if (MensajeRespuesta(res))
            {
                using (StreamReader r = new StreamReader(res.GetResponseStream()))
                {
                    string json = r.ReadToEnd();
                    Cotizacion items = JsonConvert.DeserializeObject<Cotizacion>(json);
                    return items;
                }
            }
            else return null;
        }

        HttpWebResponse GrabarCotizacion(Cotizacion cotizacion)
        {
            string postData = (new JavaScriptSerializer().Serialize(cotizacion));
            byte[] data = Encoding.UTF8.GetBytes(postData);
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://localhost:56141/Cotizaciones/");
            req.Method = "POST";
            req.ContentLength = data.Length;
            req.ContentType = "application/json";
            req.Headers.Add("Authorization", l.Oauth_token);
            using (Stream loPostData = req.GetRequestStream())
            {
                loPostData.Write(data, 0, data.Length);
            }
            return (HttpWebResponse)req.GetResponse();


        }

        HttpWebResponse GrabarPCR(PCR pcr)
        {
            string postData = (new JavaScriptSerializer().Serialize(pcr));
            byte[] data = Encoding.UTF8.GetBytes(postData);
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://localhost:56141/PCRs/");
            req.Method = "POST";
            req.ContentLength = data.Length;
            req.ContentType = "application/json";
            req.Headers.Add("Authorization", l.Oauth_token);
            using (Stream loPostData = req.GetRequestStream())
            {
                loPostData.Write(data, 0, data.Length);
            }
            return (HttpWebResponse)req.GetResponse();
        }

        HttpWebResponse EliminarPCR(PCR pcr)
        {
            string postData = (new JavaScriptSerializer().Serialize(pcr));
            byte[] data = Encoding.UTF8.GetBytes(postData);
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://localhost:56141/PCRs/");
            req.Method = "DELETE";
            req.ContentLength = data.Length;
            req.ContentType = "application/json";
            req.Headers.Add("Authorization", l.Oauth_token);
            using (Stream loPostData = req.GetRequestStream())
            {
                loPostData.Write(data, 0, data.Length);
            }
            return (HttpWebResponse)req.GetResponse();
        }

        Requerimiento CargarRequerimiento(int codigo)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(String.Format("http://localhost:56141/Requerimientos/{0}", codigo));
            req.Method = "GET";
            req.ContentType = "application/json";
            req.Headers.Add("Authorization", l.Oauth_token);
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            if (MensajeRespuesta(res))
            {
                using (StreamReader r = new StreamReader(res.GetResponseStream()))
                {
                    string json = r.ReadToEnd();
                    Requerimiento items = JsonConvert.DeserializeObject<Requerimiento>(json);
                    return items;
                }
            }
            else return null;
        }

        void CargarGrillaRequerimiento()
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://localhost:56141/Requerimientos/");
            req.Method = "GET";
            req.ContentType = "application/json";
            req.Headers.Add("Authorization", l.Oauth_token);
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            if (MensajeRespuesta(res)) {
                using (StreamReader r = new StreamReader(res.GetResponseStream()))
                {
                    string json = r.ReadToEnd();
                    List<Requerimiento> items = JsonConvert.DeserializeObject<List<Requerimiento>>(json);
                    gvRequerimientoPCR.DataSource = items;
                    gvRequerimientoPCR.DataBind();
                }
            }
        }

        void CargarGrillaPCR()
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://localhost:56141/PCRs/");
            req.Method = "GET";
            req.ContentType = "application/json";
            req.Headers.Add("Authorization", l.Oauth_token);
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            if (MensajeRespuesta(res)) {
                using (StreamReader r = new StreamReader(res.GetResponseStream()))
                {
                    string json = r.ReadToEnd();
                    List<PCR> items = JsonConvert.DeserializeObject<List<PCR>>(json);
                    gvPCR.DataSource = items;
                    gvPCR.DataBind();
                }
            }
        }

        void CargarGrillaPCR(string estado)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(String.Format("http://localhost:56141/PCRs/estado/{0}", estado));
            req.Method = "GET";
            req.ContentType = "application/json";
            req.Headers.Add("Authorization", l.Oauth_token);
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            if (MensajeRespuesta(res)) {
                using (StreamReader r = new StreamReader(res.GetResponseStream()))
                {
                    string json = r.ReadToEnd();
                    List<PCR> items = JsonConvert.DeserializeObject<List<PCR>>(json);
                    gvPCR.DataSource = items;
                    gvPCR.DataBind();
                }
            }
        }

        protected void gvPCR_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow) {
                if (ddlEstado.SelectedValue == "2")
                {
                    e.Row.FindControl("imgEditar").Visible = false;
                    e.Row.FindControl("imgEliminar").Visible = false;
                    e.Row.FindControl("imgVer").Visible = true;
                    e.Row.FindControl("imgCotizar").Visible = false;
                }
                else {
                    e.Row.FindControl("imgEditar").Visible = true;
                    e.Row.FindControl("imgEliminar").Visible = true;
                    e.Row.FindControl("imgVer").Visible = false;
                    e.Row.FindControl("imgCotizar").Visible = true;
                }
            }
        }

        protected void btCotizarGuardar_Click(object sender, EventArgs e)
        {
            try
            {

                Cotizacion cotizacion = new Cotizacion();
                cotizacion.pcr = new PCR() { idpcr = int.Parse(gvPCR.SelectedValue.ToString()) };
                cotizacion.monto = float.Parse(tbCotizarMonto.Text);
                cotizacion.descripcion = tbCotizarDescripcion.Text;
                cotizacion.fechacotizacion = DateTime.Now;
                HttpWebResponse res = GrabarCotizacion(cotizacion);
                if (MensajeRespuesta(res)) {
                    mpeCotizar.Hide();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("403"))
                    lbMensaje.Text = Mensajes.Ambar("No autorizado a consumir el servicio <a href=\"frmLogin.aspx\">Inicie sesión</a>");
                else lbMensaje.Text = Mensajes.Ambar(ex.Message);
            }

        }

        bool MensajeRespuesta(HttpWebResponse res) {
            switch (res.StatusCode)
            {
                case HttpStatusCode.Created:
                    return true;
                case HttpStatusCode.NoContent:
                    return true;
                case HttpStatusCode.OK:
                    return true;
                case HttpStatusCode.ExpectationFailed:
                    lbMensaje.Text = Mensajes.Ambar(res.StatusDescription);
                    return false;
                case HttpStatusCode.Forbidden:
                    lbMensaje.Text = Mensajes.Ambar(res.StatusDescription + " <a href=\"frmLogin.aspx\">Inicie sesión</a>");
                    return false;
                case HttpStatusCode.NotImplemented:
                    lbMensaje.Text = Mensajes.Ambar(res.StatusDescription);
                    return false;
                default:
                    lbMensaje.Text = Mensajes.Ambar("Error no controlado");
                    return false;
            }
        }

    }

    public static class Ext
    {

        public static string ToHexString(this byte[] hex)
        {
            if (hex == null)
            {
                return null;
            }
            if (hex.Length == 0)
            {
                return string.Empty;
            }
            var s = new StringBuilder();
            foreach (byte b in hex)
            {
                s.Append(b.ToString("x2"));
            }
            return s.ToString();
        }

        public static byte[] ToHexBytes(this string hex)
        {
            if (hex == null)
            {
                return null;
            }
            if (hex.Length == 0)
            {
                return new byte[0];
            }
            int l = hex.Length / 2;
            var b = new byte[l];
            for (int i = 0; i < l; ++i)
            {
                b[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }
            return b;
        }
    }
}