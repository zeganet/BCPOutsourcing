﻿using BCPWebOutsourcing.Logins1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Security;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCPWebOutsourcing
{
    public partial class frmLogin : System.Web.UI.Page
    {
        LoginsClient client;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btLogin_Click(object sender, EventArgs e)
        {

            if (client == null) { client = new LoginsClient(); }
            client.ClientCredentials.UserName.UserName = "WSBCP";
            client.ClientCredentials.UserName.Password = "REVDNTI3QTUtREU5MC00QkVGLUJDQkEtRjI5RDREMTI2QjNFU3lzdGVtLkJ5dGVbXQ";
            client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

            login l = client.ValidarSession(new BCPDominio.Usuario() { usuario = tbUsuario.Text, clave = tbClave.Text, empresa = new BCPDominio.Empresa() { idempresa = 2 } });

            if (l.Success)
            {
                Session["login"] = l;
                Response.Redirect("frmPrincipal.aspx");
            }
            else
            {
                lbMensaje.Text = Mensajes.Ambar(l.Mensaje);
                upMensaje.Update();
            }

        }
    }
}