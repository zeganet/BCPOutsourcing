﻿namespace BCPWebOutsourcing
{
    internal class Mensajes
    {
        public static string Rojo(string msj)
        {
            if (string.IsNullOrEmpty(msj)) { return ""; }
            string Mensaje = null;
            Mensaje = "<div class=\"alert error closeable border-4 glow-shadow\">";
            Mensaje += "<div class=\"alert-inner\">";
            Mensaje += "<div class=\"alert-message\">";
            Mensaje += "<p class=\"title\"><strong>Se ha producido el siguiente error:</strong></p>";
            Mensaje += "<ul>";


            Mensaje += "<li class=\"error.required\"> " + msj + " </li>";


            Mensaje += "</ul>";
            Mensaje += "</div>";
            Mensaje += "</div>";
            Mensaje += "<a class=\"alert-close\" href=\"#\" onclick=\"$(this).parent().fadeOut(250, function() { $(this).css({opacity:0}).animate({height: 0}, 100, function() { $(this).remove(); }); });\">Cerrar</a>";
            Mensaje += "</div>";
            return Mensaje;

        }

        public static string Ambar(string msj)
        {
            if (string.IsNullOrEmpty(msj)) { return ""; }
            string Mensaje = null;
            Mensaje = "<div class=\"alert caution closeable border-4 glow-shadow\">";
            Mensaje += "<div class=\"alert-inner\">";
            Mensaje += "<div class=\"alert-message\">";
            Mensaje += "<p class=\"title\"><strong>Se ha producido la siguiente Advertencia:</strong></p>";
            Mensaje += "<ul>";


            Mensaje += "<li class=\"error.required\"> " + msj + " </li>";


            Mensaje += "</ul>";
            Mensaje += "</div>";
            Mensaje += "</div>";
            Mensaje += "<a class=\"alert-close\" href=\"#\" onclick=\"$(this).parent().fadeOut(250, function() { $(this).css({opacity:0}).animate({height: 0}, 100, function() { $(this).remove(); }); });\">Cerrar</a>";
            Mensaje += "</div>";
            return Mensaje;

        }

        public static string Verde(string msj)
        {

            if (string.IsNullOrEmpty(msj)) { return ""; }
            string Mensaje = null;
            Mensaje = "<div class=\"alert success closeable border-4 glow-shadow\">";
            Mensaje += "<div class=\"alert-inner\">";
            Mensaje += "<div class=\"alert-message\">";
            Mensaje += "<p class=\"title\"><strong>" + msj + "</strong></p>";
            Mensaje += "<ul>";


            Mensaje += "<li class=\"error.required\"> " + msj + " </li>";


            Mensaje += "</ul>";
            Mensaje += "</div>";
            Mensaje += "</div>";
            Mensaje += "<a class=\"alert-close\" href=\"#\" onclick=\"$(this).parent().fadeOut(250, function() { $(this).css({opacity:0}).animate({height: 0}, 100, function() { $(this).remove(); }); });\">Cerrar</a>";
            Mensaje += "</div>";
            return Mensaje;

        }

    }
}
