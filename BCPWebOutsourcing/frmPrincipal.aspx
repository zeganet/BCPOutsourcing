﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master.Master" AutoEventWireup="true" CodeBehind="frmPrincipal.aspx.cs" Inherits="BCPWebOutsourcing.frmPrincipal" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 111px;
        }

        .auto-style2 {
            width: 48px;
        }

        .auto-style5 {
            width: 139px;
        }

        .auto-style6 {
            width: 7px;
        }
    </style>

    <script type="text/javascript">
        //$get("FileArchivoTecnico").onchange = function () {
        //    //$get("uploadFile").value = this.value;
        //    uploadFile(this.files[0]);
        //};
        function uploadFile(f) {
            //5MB
            file = f.files[0];
            var limit = 1048576 * 2, xhr;

            console.log(limit);

            if (file) {
                if (file.size < limit) {
                    //if (!confirm('Cargar archivo?')) { return; }

                    xhr = new XMLHttpRequest();

                    xhr.onreadystatechange = function (e) {
                        if (xhr.readyState == 4) {
                            tbArchivo.value = file.name;
                        }
                    };

                    xhr.upload.addEventListener('load', function (e) {
                        //alert('Archivo cargado!');

                    }, false);

                    xhr.upload.addEventListener('error', function (e) {
                        alert('Ha habido un error :/');
                    }, false);

                    xhr.open('POST', 'Cargar.ashx');

                    xhr.setRequestHeader("Cache-Control", "no-cache");
                    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                    xhr.setRequestHeader("X-File-Name", file.name);

                    xhr.send(file);
                } else {
                    alert('El archivo es mayor que 2MB!');
                }
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="App_Themes/Grid/grid.css" rel="stylesheet" />
    <link href="App_Themes/Modal/modal.css" rel="stylesheet" />
    <link href="App_Themes/css/Mensajes.css" rel="stylesheet" />
    <script src="App_Themes/js/jquery-1.4.2.min.js"></script>
    <table style="width: 100%; background-color:black;">
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <table style="width: 100%; margin-top: 25px">
        <tr>
            <td></td>
            <td style="width: 25%" valign="top">
                <div class="card" style="width: 85%">
                    <div class="cardtext">
                        Información General
                    </div>
                    <div class="cardcuerpo" style="display: block; height: auto; overflow-x: auto;">
                        <div style="margin: 0px 8px 0px 8px;">
                            <br />
                            <table style="width: 100%;">
                                <tr>
                                    <td><b>Usuario</b></td>
                                    <td align="right">
                                        <asp:LinkButton ID="btSessionCerrar" runat="server" OnClick="btSessionCerrar_Click">Salir</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            <hr />
                            <table style="width: 100%;">
                                <tr>
                                    <td colspan="3" align="center">
                                        <div class="fileUpload">
                                            <span>
                                                <img id="Image1" runat="server" src="App_Themes/css/images/generico.png" style="border: 1px solid #dcd9d9; padding: 6px; width: 100px; height: 100px" />
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <b>Personal </b>
                            <hr />
                            <table style="width: 100%; height: 90px;" border="0">
                                <tr>
                                    <td valign="top">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td>Usuario : </td>
                                                <td>
                                                    <asp:Label ID="lbUsuario" runat="server" Text="" ForeColor="#000066"></asp:Label>
                                                </td>
                                                <td>&nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td>Nombre Completo : </td>
                                                <td>
                                                    <asp:Label ID="lbApellidosNombre" runat="server" Text="" ForeColor="#000066"></asp:Label>
                                                </td>
                                                <td>&nbsp; </td>
                                            </tr>
                                            <tr>
                                                <td>Hora Acceso: </td>
                                                <td>
                                                    <asp:Label ID="lbHoraAcceso" runat="server" Text="" ForeColor="#000066"></asp:Label>
                                                </td>
                                                <td>&nbsp; </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </td>
            <td valign="top">
                <div class="card" style="width: 85%">
                    <div class="cardtext">
                        Lista de requerimientos
                    </div>
                    <div class="cardcuerpo" style="display: block; height: auto; overflow-x: auto;">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvRequerimientoPCR" runat="server" CssClass="grid" DataKeyNames="idrequerimiento" AutoGenerateColumns="false" OnRowCommand="gvRequerimientoPCR_OnRowCommand">
                                    <Columns>
                                        <asp:BoundField HeaderText="Fecha Solicitud" DataField="fechasolicitud" />
                                        <asp:BoundField HeaderText="Motivo" DataField="motivo" />
                                        <asp:BoundField HeaderText="Detalle" DataField="detalle" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgCrearPCR" runat="server" ImageUrl="~/App_Themes/css/images/crear.jpg" Width="20px" CommandName="CrearPCR" CommandArgument="<%# Container.DataItemIndex%>" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <center><b>No tiene pendientes a generar</b></center>
                                    </EmptyDataTemplate>
                                </asp:GridView>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <br />
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lbMensaje" runat="server" Text="" ></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <div class="cardtext">
                        Lista de PCR
                    </div>
                    <div class="cardcuerpo" style="display: block; height: auto; overflow-x: auto;">
                        <b>Filtros</b>
                        <hr />  
                        <table style="width: 100%;">
                            <tr>
                                <td class="auto-style2">Estado</td>
                                <td class="auto-style1">
                                    <asp:DropDownList ID="ddlEstado" runat="server" Width="100px">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btBuscar" runat="server" Text="Buscar" OnClick="btBuscar_Click" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                            </tr>
                        </table>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvPCR" runat="server" CssClass="grid" DataKeyNames="idpcr" AutoGenerateColumns="false" OnRowCommand="gvPCR_OnRowCommand" OnRowDataBound="gvPCR_RowDataBound">
                                    <Columns>
                                        <asp:BoundField HeaderText="Version" DataField="version" />
                                        <asp:BoundField HeaderText="Detalle" DataField="detalle" />
                                        <asp:BoundField HeaderText="Descripcion" DataField="descripcion" />
                                        <asp:BoundField HeaderText="Fecha de registro" DataField="fecharegistro" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgCotizar" runat="server" ImageUrl="~/App_Themes/css/images/cotizar.png" Width="20px" CommandName="CotizarPCR" CommandArgument="<%# Container.DataItemIndex%>" />
                                            </ItemTemplate>
                                            <ItemStyle Width="20px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEditar" runat="server" ImageUrl="~/App_Themes/css/images/editar.png" Width="20px" CommandName="EditarPCR" CommandArgument="<%# Container.DataItemIndex%>" />
                                                <asp:ImageButton ID="imgVer" runat="server" ImageUrl="~/App_Themes/css/images/ver.png" Width="20px" CommandName="VerPCR" CommandArgument="<%# Container.DataItemIndex%>" />
                                            </ItemTemplate>
                                            <ItemStyle Width="20px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEliminar" runat="server" ImageUrl="~/App_Themes/css/images/eliminar.png" Width="20px" CommandName="ElimnarPCR" CommandArgument="<%# Container.DataItemIndex%>" />
                                            </ItemTemplate>
                                            <ItemStyle Width="20px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <center><b>No hay datos a mostrar</b></center>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>



                    </div>
                </div>
            </td>
        </tr>
    </table>



    <asp:ModalPopupExtender ID="mpeRequerimiento" runat="server"
        BackgroundCssClass="CssFondo" TargetControlID="lbRequerimientotmp" PopupControlID="divRequerimiento" CancelControlID="btRequerimientoCerrar">
    </asp:ModalPopupExtender>
    <asp:Label ID="lbRequerimientotmp" runat="server" Text=""></asp:Label>
    <div id="divRequerimiento" class="card" style="width: 70%">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="cardtext">
                    <asp:Label ID="lbPCRTitulo" runat="server" Text=""></asp:Label>
                </div>
                <div class="cardcuerpo">
                    <table style="width: 100%;">
                        <tr>
                            <td class="auto-style5">Version</td>
                            <td class="auto-style6">&nbsp;</td>
                            <td>
                                <asp:Label ID="lbVersion" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="auto-style5">Fecha de Solicitud</td>
                            <td class="auto-style6">&nbsp;</td>
                            <td>
                                <asp:Label ID="lbFechaSolicitud" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="auto-style5">Motivo del Requerimiento</td>
                            <td class="auto-style6">&nbsp;</td>
                            <td>
                                <asp:Label ID="lbMotivo" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="auto-style5">Detalle del Requerimiento</td>
                            <td class="auto-style6">&nbsp;</td>
                            <td>
                                <asp:Label ID="lbDetalleRequerimiento" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="auto-style5">Solicitante</td>
                            <td class="auto-style6">&nbsp;</td>
                            <td>
                                <asp:Label ID="lbSolicitante" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="auto-style5">Responsable GSTI</td>
                            <td class="auto-style6">&nbsp;</td>
                            <td>
                                <asp:Label ID="lbResponsableGSTI" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <%--<tr>
                            <td>Responsable Outsourcing</td>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Label ID="lbResponsableOutsourcing" runat="server" Text=""></asp:Label></td>
                        </tr>--%>
                        <tr>
                            <td class="auto-style5">Resumen PCR</td>
                            <td class="auto-style6">&nbsp;</td>
                            <td>
                                <asp:TextBox ID="tbPCRResumen" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style5">Descripcion PCR</td>
                            <td class="auto-style6">&nbsp;</td>
                            <td>
                                <asp:TextBox ID="tbPCRDescripcion" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style5">Responsable IBM</td>
                            <td class="auto-style6">&nbsp;</td>
                            <td>
                                <asp:Label ID="lbResponsableIBM" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="auto-style5">Archivo Técnico</td>
                            <td class="auto-style6">&nbsp;</td>
                            <td>
                                <input id="FileArchivoTecnico" runat="server" type="file" onchange="uploadFile(this)" />
                                <asp:HyperLink ID="hlArchivo" runat="server"></asp:HyperLink>
                                <asp:TextBox ID="tbArchivo" runat="server" ClientIDMode="Static" CssClass="invisible"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table style="width: 100%;">
            <tr>
                <td>

                    <asp:UpdatePanel ID="up3" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btRequerimientoGrabar" runat="server" Text="Grabar" OnClick="btRequerimientoGrabar_Click" />
                            <asp:Button ID="btPCRActualizar" runat="server" Text="Editar" OnClick="btPCRActualizar_Click" />
                        </ContentTemplate>
                        <%--<Triggers>
                            <asp:PostBackTrigger ControlID="btRequerimientoGrabar" />
                            <asp:PostBackTrigger ControlID="btPCRActualizar" />
                        </Triggers>--%>
                    </asp:UpdatePanel>
                </td>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btRequerimientoCerrar" runat="server" Text="Cerrar" /></td>
            </tr>
        </table>

    </div>



    <asp:ModalPopupExtender ID="mpeCotizar" runat="server"
        BackgroundCssClass="CssFondo" TargetControlID="lbCotizartmp" PopupControlID="divCotizar" CancelControlID="btCotizarCerrar">
    </asp:ModalPopupExtender>
    <asp:Label ID="lbCotizartmp" runat="server" Text=""></asp:Label>

    <div id="divCotizar" class="card" style="width: 30%">
        <div class="cardtext">
            Cotizar
        </div>
        <div class="cardcuerpo">
            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                <ContentTemplate>
                    <table style="width: 100%;">
                        <tr>
                            <td>Monto</td>
                            <td>&nbsp;</td>
                            <td>
                                <asp:TextBox ID="tbCotizarMonto" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Descripción</td>
                            <td>&nbsp;</td>
                            <td>
                                <asp:TextBox ID="tbCotizarDescripcion" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </ContentTemplate>
                
            </asp:UpdatePanel>

            <table style="width: 100%;">
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btCotizarGuardar" runat="server" Text="Guardar Cotización" OnClick="btCotizarGuardar_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <asp:Button ID="btCotizarCerrar" runat="server" Text="Cerrar" /></td>
                </tr>
            </table>



        </div>
    </div>




</asp:Content>
