﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace BCPWebOutsourcing
{
    /// <summary>
    /// Descripción breve de Cargar
    /// </summary>
    public class Cargar : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string fileName = context.Request.ServerVariables["HTTP_X_FILE_NAME"];
                FileStream newFile = new FileStream(context.Server.MapPath("~") + "\\FilesUpload\\" + fileName, FileMode.Create);
                byte[] body = context.Request.BinaryRead(context.Request.TotalBytes);
                newFile.Write(body, 0, body.Length);
                newFile.Flush();
                newFile.Close();
                context.Response.Write("{success: true}");
            }
            catch (Exception)
            {
                context.Response.Write("{success: false}");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}