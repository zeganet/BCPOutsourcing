﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmLogin.aspx.cs" Inherits="BCPWebOutsourcing.frmLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Exo:100,200,400);
        @import url(https://fonts.googleapis.com/css?family=Source+Sans+Pro:700,400,300);

        body {
            margin: 0;
            padding: 0;
            background: #fff;
            width: 100%;
            height: 100%;
            color: #fff;
            font-family: Arial;
            font-size: 12px;
            background-image: url(App_Themes/css/images/bussiness.jpg);
        }


        .header div {
            float: left;
            color: #fff;
            font-family: 'Exo', sans-serif;
            font-size: 35px;
            font-weight: 200;
            margin-left: 15px;
        }

            .header div span {
                color: #FF6A4C !important;
                font-weight: bold;
            }

        .login {
            padding: 50px;
            z-index: 2;
            width: 600px;
            background-color: rgba(255,255,255,0.6);
            margin-top: 150px;
            margin-left:28%;
        }

            .login input[type=text] {
                width: 250px;
                height: 30px;
                background: transparent;
                border: 1px solid rgba(255,255,255,0.6);
                border-radius: 2px;
                color: #fff;
                font-family: 'Exo', sans-serif;
                font-size: 16px;
                font-weight: 400;
                padding: 4px;
                margin-top: 10px;
                margin-bottom: 10px;
            }

            .login input[type=password] {
                width: 250px;
                height: 30px;
                background: transparent;
                border: 1px solid rgba(255,255,255,0.6);
                border-radius: 2px;
                color: #fff;
                font-family: 'Exo', sans-serif;
                font-size: 16px;
                font-weight: 400;
                padding: 4px;
                margin-top: 10px;
                margin-bottom: 10px;
            }

            .login label {
                width: 250px;
                height: 30px;
                color: #fff;
                font-family: 'Exo', sans-serif;
                font-size: 16px;
                font-weight: 400;
                padding: 4px;
                margin-top: 10px;
                vertical-align: middle;
            }

            .login input[type=submit] {
                width: 260px;
                height: 35px;
                background: #fff;
                border: 1px solid #fff;
                cursor: pointer;
                border-radius: 2px;
                color: #a18d6c;
                font-family: 'Exo', sans-serif;
                font-size: 16px;
                font-weight: 400;
                padding: 6px;
                margin-top: 10px;
            }

                .login input[type=submit]:hover {
                    opacity: 0.8;
                }

                .login input[type=submit]:active {
                    opacity: 0.6;
                }

            .login input[type=text]:focus {
                outline: none;
                border: 1px solid rgba(255,255,255,0.9);
            }

            .login input[type=password]:focus {
                outline: none;
                border: 1px solid rgba(255,255,255,0.9);
            }

            .login input[type=submit]:focus {
                outline: none;
            }

        ::-webkit-input-placeholder {
            color: rgba(255,255,255,0.6);
        }

        ::-moz-input-placeholder {
            color: rgba(255,255,255,0.6);
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <link href="App_Themes/css/Mensajes.css" rel="stylesheet" />
        <script src="App_Themes/js/jquery-1.4.2.min.js"></script>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <div class="login">
                <table style="width: 100%">
                    <tr>
                        <td>
                            <div class="header">
                                <div>SISTEMAS
                                    <br>
                                    <span>BCP - IBM</span></div>
                            </div>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        Usuario:<br /><asp:TextBox ID="tbUsuario" runat="server" requerid></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Contraseña:<br /><asp:TextBox ID="tbClave" runat="server" Text="" required TextMode="Password"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                            <ProgressTemplate>
                                                <img src="App_Themes/css/images/cargando.gif" />
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                        <%--<asp:CheckBox ID="cbRecordarContraseña" runat="server" Text="Guardar contraseña" />--%>
                                        <asp:UpdatePanel ID="upMensaje" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="lbMensaje" runat="server" Text=""></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Button ID="btLogin" runat="server" Text="Entrar" OnClick="btLogin_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
