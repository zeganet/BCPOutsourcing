﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace BCPDominio
{
    [DataContract]
    public class Requerimiento
    {
        [DataMember]
        public int idrequerimiento { get; set; }
        [DataMember]
        public Usuario usuariosolicitante { get; set; }
        [DataMember]
        public DateTime fechasolicitud { get; set; }
        [DataMember]
        public string motivo { get; set; }
        [DataMember]
        public string detalle { get; set; }
        [DataMember]
        public Usuario usuarioGSTI { get; set; }
        [DataMember]
        public DateTime validaGSTI { get; set; }
        [DataMember]
        public Usuario usuarioOutsourcing { get; set; }
        [DataMember]
        public DateTime validaOutsourcing { get; set; }
        [DataMember]
        public string estado { get; set; }
    }
}