﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BCPDominio
{
    [DataContract]
    public class Validacion
    {
        [DataMember]
        public int idvalidacion { get; set; }
        [DataMember]
        public ValidacionTipo validaciontipo { get; set; }
        [DataMember]
        public PCR pcr { get; set; }
        [DataMember]
        public string valida { get; set; }
        [DataMember]
        public DateTime fechavalida { get; set; }
    }
}
