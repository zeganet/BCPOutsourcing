﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BCPDominio
{
    [DataContract]
    public class Cotizacion
    {
        [DataMember]
        public int idcotizacion { get; set; }
        [DataMember]
        public PCR pcr { get; set; }
        [DataMember]
        public float monto { get; set; }
        [DataMember]
        public string descripcion { get; set; }
        [DataMember]
        public DateTime fechacotizacion { get; set; }
    }
}
