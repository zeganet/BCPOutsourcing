﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace BCPDominio
{
    [DataContract]
    public class Servicio
    {
        [DataMember]
        public int idservicio { get; set; }
        [DataMember]
        public Categoria categoria { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public Usuario usuarioregistra { get; set; }
        [DataMember]
        public DateTime fecharegistro { get; set; }
    }
}