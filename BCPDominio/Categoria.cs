﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BCPDominio
{
    [DataContract]
    public class Categoria
    {
        [DataMember]
        public int idcategoria { get; set; }
        [DataMember]
        public string nombre { get; set; }
    }
}