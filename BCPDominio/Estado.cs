﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BCPDominio
{
    [DataContract]
    public class Estado
    {
        [DataMember]
        public int idestado { get; set; }
        [DataMember]
        public string nombre { get; set; }
    }
}
