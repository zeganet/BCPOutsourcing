﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BCPDominio
{
    [DataContract]
    public class Usuario
    {
        [DataMember]
        public int idusuario { get; set; }
        [DataMember]
        public string usuario { get; set; }
        [DataMember]
        public string clave { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public string apellidopaterno { get; set; }
        [DataMember]
        public string apellidomaterno { get; set; }
        [DataMember]
        public DateTime fechanacimiento { get; set; }
        [DataMember]
        public Empresa empresa { get; set; }
        [DataMember]
        public  Area area { get; set; }
        [DataMember]
        public Cargo cargo { get; set; }
        [DataMember]
        public string estado { get; set; }
        [DataMember]
        public DateTime fecharegistro { get; set; }
    }
}
