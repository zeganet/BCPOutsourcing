﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BCPDominio
{
    [DataContract]
    public class PCR
    {
        [DataMember]
        public int idpcr { get; set; }
        [DataMember]
        public Requerimiento requerimiento { get; set; }
        [DataMember]
        public Estado estado { get; set; }
        [DataMember]
        public float version { get; set; }
        [DataMember]
        public string detalle { get; set; }
        [DataMember]
        public string descripcion { get; set; }
        [DataMember]
        public Usuario usuarioIBM { get; set; }
        [DataMember]
        public Usuario usuarioOutsourcing { get; set; }
        [DataMember]
        public string adjuntotecnico { get; set; }
        [DataMember]
        public DateTime fecharegistro { get; set; }
        [DataMember]
        public string activo { get; set; }

        public override string ToString()
        {
            return detalle;
        }
    }
}
