﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BCPDominio
{
    [DataContract]
    public class ValidacionTipo
    {
        [DataMember]
        public int idvalidaciontipo { get; set; }
        [DataMember]
        public string nombre { get; set; }
        public override string ToString() {
            return nombre;
        }

    }
}
