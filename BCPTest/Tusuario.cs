﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BCPTest.Users1;
using System.Net;
using System.IO;

namespace BCPTest
{
    [TestClass]
    public class Tusuario
    {
        [TestMethod]
        public void TestCrear()
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://localhost:56141/Estados/");
            req.Method = "GET";
            req.ContentType = "application/json";
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            using (StreamReader r = new StreamReader(res.GetResponseStream()))
            {
                string json = r.ReadToEnd();

            }

            Assert.AreEqual(res.StatusCode, 200);
        }

        [TestMethod]
        public void TestObtener()
        {
            IUsers u = new UsersClient();
            User checkUser = u.ObtenerUsuario(1);
            Assert.AreEqual(checkUser.username, "zeganet@gmail.com");
        }

        [TestMethod]
        public void TestEliminar()
        {
            IUsers u = new UsersClient();
            u.EliminarUsuario(new User() { iduser=3 });
            User checkUser = u.ObtenerUsuario(3);
            Assert.IsNull(checkUser);
        }

        [TestMethod]
        public void TestListarUsuarios()
        {
            IUsers u = new UsersClient();
            User[] checkUsers = u.ListarUsuarios();
            CollectionAssert.AllItemsAreNotNull(checkUsers);
        }

        [TestMethod]
        public void TestValidarUsuario()
        {
            IUsers u = new UsersClient();
            User checkUser = u.ValidarUsuario(new User(){ username= "zeganet@gmail.com", password= "123456" });
            Assert.IsNotNull(checkUser);
        }

    }
}
