﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BCPTest.Logins1;
using System.ServiceModel.Security;

namespace BCPTest
{
    [TestClass]
    public class Tlogin
    {
        [TestMethod]
        public void TestIBM()
        {
            //ILogins client;
            var client = new LoginsClient();

            client.ClientCredentials.UserName.UserName = "WSBCP";
            client.ClientCredentials.UserName.Password = "REVDNTI3QTUtREU5MC00QkVGLUJDQkEtRjI5RDREMTI2QjNFU3lzdGVtLkJ5dGVbXQ";
            client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

            login l = client.ValidarSession(new Usuario() { usuario = "usuario", clave = "usuario", empresa = new Empresa() { idempresa = 1 } });
            Assert.IsTrue(l.Success);
        }
        [TestMethod]
        public void TestBCP()
        {
            //ILogins client;
            login l;
            var client = new LoginsClient();

            client.ClientCredentials.UserName.UserName = "WSBCP";
            client.ClientCredentials.UserName.Password = "REVDNTI3QTUtREU5MC00QkVGLUJDQkEtRjI5RDREMTI2QjNFU3lzdGVtLkJ5dGVbXQ";
            client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

            l = client.ValidarSession(new Usuario() { usuario = "usuario", clave = "usuario", empresa = new Empresa() { idempresa = 1 } });
            Assert.IsTrue(l.Success);
        }

    }
}
