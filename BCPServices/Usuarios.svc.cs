﻿using AutoMapper;
using BCPDominio;
using BCPServices.Persistencia;
using Simple.Web.Links;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BCPServices
{
    public class Usuarios : IUsuarios
    {

        #region "Fabricas"
        private UsuarioDAO usuarioDAO = null;
        private UsuarioDAO UsuarioDAO
        {
            get
            {
                if (usuarioDAO == null)
                    usuarioDAO = new UsuarioDAO();
                return usuarioDAO;
            }
        }
        #endregion

        public Usuario CrearUsuario(Usuario items)
        {
            return UsuarioDAO.Crear(items);
        }

        public Usuario ObtenerUsuario(string codigo)
        {
            try
            {
                Usuario usuario = UsuarioDAO.Obtener(int.Parse(codigo));
                if (usuario == null)
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NoContent;
                    ctx.OutgoingResponse.StatusDescription = "El codigo ingresado no existe";
                }
                else
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                    ctx.OutgoingResponse.StatusDescription = "Atendido";
                }
                return usuario;
            }
            catch 
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                ctx.OutgoingResponse.StatusDescription = "Revise los datos ingresado e intentelo nuevamente";
                return null;
            }

        }

        public Usuario ModificarUsuario(Usuario items)
        {
            return UsuarioDAO.Modificar(items);
        }

        public void EliminarUsuario(Usuario items)
        {
            UsuarioDAO.Eliminar(items);
        }
        public List<Usuario> ListarUsuarios()
        {
            return UsuarioDAO.ListarTodos().ToList();
        }

        public Usuario ValidarUsuario(Usuario items)
        {
            Usuario usuario =  UsuarioDAO.GetByUserName(items);
            if (usuario != null)
                if (usuario.clave.Equals(items.clave))
                    return usuario;
                else
                    return null;
            return null;
        }
    }
}

