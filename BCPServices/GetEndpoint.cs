﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Web;
using Simple.Web.Behaviors;
using Simple.Web.Links;
namespace BCPServices
{
    [UriTemplate("/")]
    public class GetEndpoint : IGet, IOutput<IEnumerable<Link>>
    {
        public Status Get()
        {
            this.Output = LinkHelper.GetRootLinks();

            return 200;
        }

        public IEnumerable<Link> Output { get; private set; }
    }
}


