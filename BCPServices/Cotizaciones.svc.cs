﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BCPDominio;
using BCPServices.Persistencia;
using System.ServiceModel.Web;

namespace BCPServices
{

    public class Cotizaciones : ICotizacion
    {
        #region "Fabricas"
        private CotizacionDAO cotizacionDAO = null;
        private CotizacionDAO CotizacionDAO
        {
            get
            {
                if (cotizacionDAO == null)
                    cotizacionDAO = new CotizacionDAO();
                return cotizacionDAO;
            }
        }
        #endregion

        public Cotizacion Create(Cotizacion items)
        {
            try
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Created;
                ctx.OutgoingResponse.StatusDescription = "PCR Creado correctamente";
                items = CotizacionDAO.Crear(items);

                return items;
            }
            catch
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                ctx.OutgoingResponse.StatusDescription = "Revise los datos ingresado e intentelo nuevamente";
                return null;
            }
        }

        public void Delete(Cotizacion pcr)
        {
            try
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                ctx.OutgoingResponse.StatusDescription = "PCR Eliminado correctamente";
                //pcr = CotizacionDAO.Obtener(pcr.idpcr);
                //CotizacionDAO.EliminarPCR(pcr);
            }
            catch
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                ctx.OutgoingResponse.StatusDescription = "Revise los datos ingresado e intentelo nuevamente";
            }
        }

        public Cotizacion Read(string codigo)
        {
            try
            {
                Cotizacion cotizacion = CotizacionDAO.Obtener(int.Parse(codigo));
                if (cotizacion == null)
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NoContent;
                    ctx.OutgoingResponse.StatusDescription = "El codigo ingresado no existe";
                }
                else
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                    ctx.OutgoingResponse.StatusDescription = "Atendido";
                }
                return cotizacion;
            }
            catch
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                ctx.OutgoingResponse.StatusDescription = "Revise los datos ingresado e intentelo nuevamente";
                return null;
            }
        }

        public Cotizacion ByPCR(string codigo)
        {
            try
            {
                Cotizacion cotizacion = CotizacionDAO.ObtenerByPCR(int.Parse(codigo));
                if (cotizacion == null)
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NoContent;
                    ctx.OutgoingResponse.StatusDescription = "El codigo ingresado no existe";
                }
                else
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                    ctx.OutgoingResponse.StatusDescription = "Atendido";
                }
                return cotizacion;
            }
            catch(Exception e)
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                ctx.OutgoingResponse.StatusDescription = "Revise los datos ingresado e intentelo nuevamente";
                return null;
            }
        }

        public List<Cotizacion> ReadAll()
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
            ctx.OutgoingResponse.StatusDescription = "Atendido";
            return CotizacionDAO.Listar().ToList();
        }

        public Cotizacion Update(Cotizacion items)
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotImplemented;
            ctx.OutgoingResponse.StatusDescription = "No implementado";
            return null;
        }
    }
}
