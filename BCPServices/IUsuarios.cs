﻿using BCPDominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
namespace BCPServices
{
    [ServiceContract]
    public interface IUsuarios
    {
        [OperationContract]
        Usuario CrearUsuario(Usuario items);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "{codigo}")]
        Usuario ObtenerUsuario(string codigo);

        [OperationContract]
        Usuario ModificarUsuario(Usuario items);

        [OperationContract]
        void EliminarUsuario(Usuario items);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "")]
        List<Usuario> ListarUsuarios();

        [OperationContract]
        Usuario ValidarUsuario(Usuario items);
    }
}   
