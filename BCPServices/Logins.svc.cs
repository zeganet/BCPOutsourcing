﻿using BCPLoginServices;
using BCPDominio;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using System;
using BCPServices.Persistencia;

namespace BCPServices
{
    public class Logins : ILogins
    {
        ILoginService login1;

        private BaseDato basedato = null;
        private BaseDato BaseDato
        {
            get
            {
                if (basedato == null)
                    basedato = new BaseDato();
                return basedato;
            }
        }

        public login ValidarSession(Usuario usuario) {

            Usuario u = new Usuarios().ValidarUsuario(usuario);
            if (u != null)
            {
                if (u.empresa.idempresa != usuario.empresa.idempresa) {
                    return new login(false, "No tiene asignado los permisos para iniciar session en este aplicativo");
                }
                string token = GetSHA1(DateTime.Now.ToLongTimeString() + u.usuario + new Random().Next(0,100).ToString());
                BaseDato.AgregarParametro("@idusuario", u.idusuario.ToString());
                BaseDato.AgregarParametro("@token", token);
                BaseDato.Ejecutar("OUT_Acceso_Insert", BaseDato.TipoEjecucion.NoDevuelveNada, System.Data.CommandType.StoredProcedure);
                if (BaseDato.EsError) { return new login(false, BaseDato.Resultado); }
                return new login(true, "") { Idusuario = u.idusuario, Usuario = u.usuario, Mensaje = u.nombre + " " + u.apellidopaterno + " " + u.apellidomaterno, Oauth_token = token };
            }
            else
                return new login(false, "Acceso incorrecto");

            //if (login1 == null) { login1 = new LoginService(); }
            //if (usuario.empresa.idempresa == 2)
            //{
            //    Usuario u = new Usuarios().ValidarUsuario(usuario);
            //    if (u != null)
            //        return new login(true, "") { Idusuario = u.idusuario, Usuario = u.usuario, Mensaje = u.nombre + " " + u.apellidopaterno + " " + u.apellidomaterno, Oauth_token = GetSHA1(DateTime.Now.ToShortTimeString() + u.usuario) };
            //    else
            //        return new login(false, "Acceso incorrecto");
            //}
            //else
            //{
            //    login l = login1.ValidarSession(usuario.usuario, usuario.clave);
            //    if (l.Success)
            //    {
            //        Usuario u = new Usuarios().ValidarUsuario(usuario);
            //        if (u != null)
            //            return new login(true, "") { Idusuario = u.idusuario, Usuario = u.usuario, Mensaje = u.nombre + " " + u.apellidopaterno + " " + u.apellidomaterno, Oauth_token = GetSHA1(DateTime.Now.ToShortTimeString() + u.usuario) };
            //        else
            //            return new login(false, "No tiene asignado los permisos para iniciar session en este aplicativo");
            //    }
            //    return l;
            //}
        }

        public static string GetSHA1(string str)
        {
            SHA1 sha1 = SHA1Managed.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = sha1.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }

    }

}
