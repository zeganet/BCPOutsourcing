﻿using BCPDominio;
using BCPServices.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BCPServices
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Requerimientos" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Requerimientos.svc o Requerimientos.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Requerimientos : IRequerimientos
    {
        #region "Fabricas"
        private RequerimientoDAO requerimientoDAO = null;
        private RequerimientoDAO RequerimientoDAO
        {
            get
            {
                if (requerimientoDAO == null)
                    requerimientoDAO = new RequerimientoDAO();
                return requerimientoDAO;
            }
        }

        public Requerimiento Create(Requerimiento items)
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotImplemented;
            ctx.OutgoingResponse.StatusDescription = "El metodo no esta implementado por medidas de seguridad";
            return null;
        }

        public void Delete(string codigo)
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotImplemented;
            ctx.OutgoingResponse.StatusDescription = "El metodo no esta implementado por medidas de seguridad";
        }
        #endregion

        public Requerimiento Crear(Requerimiento items) {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotImplemented;
            ctx.OutgoingResponse.StatusDescription = "El metodo no esta implementado por medidas de seguridad";
            return null;
        }

        public Requerimiento Read(string codigo)
        {
            try
            {
                Requerimiento requerimiento = RequerimientoDAO.Obtener(int.Parse(codigo));
                if (requerimiento == null)
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NoContent;
                    ctx.OutgoingResponse.StatusDescription = "El codigo ingresado no existe";
                }
                else
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                    ctx.OutgoingResponse.StatusDescription = "Atendido";
                }
                return requerimiento;
            }
            catch
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                ctx.OutgoingResponse.StatusDescription = "Revise los datos ingresado e intentelo nuevamente";
                return null;
            }
        }

        public List<Requerimiento> ReadAll()
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
            ctx.OutgoingResponse.StatusDescription = "Atendido";
            return RequerimientoDAO.byEstado("AO").ToList();
        }

        public Requerimiento Update(Requerimiento items)
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotImplemented;
            ctx.OutgoingResponse.StatusDescription = "El metodo no esta implementado por medidas de seguridad";
            return null;
        }
        public void Delete(Requerimiento items) {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotImplemented;
            ctx.OutgoingResponse.StatusDescription = "El metodo no esta implementado por medidas de seguridad";
        }

    }
}
