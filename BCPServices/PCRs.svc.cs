﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BCPDominio;
using BCPServices.Persistencia;
using System.ServiceModel.Web;

namespace BCPServices
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "PCRs" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione PCRs.svc o PCRs.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class PCRs : IPCRs
    {
        #region "Fabricas"
        private PCRDAO pcrDAO = null;
        private PCRDAO PCRDAO
        {
            get
            {
                if (pcrDAO == null)
                    pcrDAO = new PCRDAO();
                return pcrDAO;
            }
        }
        #endregion

        public List<PCR> ByEstado(string estado)
        {
            try
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                ctx.OutgoingResponse.StatusDescription = "Atendido";
                List<PCR> pcrs = PCRDAO.ByEstado(int.Parse(estado),"A").ToList();
                if (pcrs.Count == 0) {
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NoContent;
                    ctx.OutgoingResponse.StatusDescription = "No existen PCR con el estado indicado";
                }
                return pcrs;
            }
            catch 
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                ctx.OutgoingResponse.StatusDescription = "Revise los datos ingresado e intentelo nuevamente";
                return null;
            }
        }

        public PCR Create(PCR items)
        {
            try
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Created;
                ctx.OutgoingResponse.StatusDescription = "PCR Creado correctamente";
                items = PCRDAO.CrearVersion(items);

                return items;
            }
            catch
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                ctx.OutgoingResponse.StatusDescription = "Revise los datos ingresado e intentelo nuevamente";
                return null;
            }
        }

        public void Delete(PCR pcr)
        {
            try
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                ctx.OutgoingResponse.StatusDescription = "PCR Eliminado correctamente";
                pcr = PCRDAO.Obtener(pcr.idpcr);
                PCRDAO.EliminarPCR(pcr);
            }
            catch
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                ctx.OutgoingResponse.StatusDescription = "Revise los datos ingresado e intentelo nuevamente";
            }
        }

        public PCR Read(string codigo)
        {
            try
            {
                PCR pcr = PCRDAO.Obtener(int.Parse(codigo));
                if (pcr == null)
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NoContent;
                    ctx.OutgoingResponse.StatusDescription = "El codigo ingresado no existe";
                }
                else
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                    ctx.OutgoingResponse.StatusDescription = "Atendido";
                }
                return pcr;
            }
            catch
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                ctx.OutgoingResponse.StatusDescription = "Revise los datos ingresado e intentelo nuevamente";
                return null;
            }
        }

        public List<PCR> ReadAll()
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
            ctx.OutgoingResponse.StatusDescription = "Atendido";
            return PCRDAO.ByEstado(1,"A").ToList();
        }

        public PCR Update(PCR items)
        {
            try
            {
                return PCRDAO.Modificar(items);
            }
            catch{
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.ExpectationFailed;
                ctx.OutgoingResponse.StatusDescription = "Revise los datos ingresado e intentelo nuevamente";
                return null;
            }
                    
            
        }
    }
}
