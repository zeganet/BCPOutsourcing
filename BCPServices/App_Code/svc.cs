﻿using BCPServices.Persistencia;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace BCPServices.App_Code
{
    class svc : IHttpModule
    {
        public void Dispose()
        {
        }
        public void Init(HttpApplication context)
        {

            context.BeginRequest += delegate
            {

                HttpContext cxt = HttpContext.Current;

                if (!Authenticate(cxt))
                {
                    cxt.Response.AddHeader("WWW-Authenticate", "Personalized realm=http://localhost:56141/Logins.svc");
                    cxt.Response.StatusCode = 403;
                    cxt.Response.StatusDescription = "No tiene permisos para ver esta pagina  ";
                }

                string path = cxt.Request.AppRelativeCurrentExecutionFilePath;
                int i = path.IndexOf('/', 2);
                if (i > 0)
                {
                    string a = path.Substring(0, i) + ".svc";
                    string b = path.Substring(i, path.Length - i);
                    string c = cxt.Request.QueryString.ToString();
                    cxt.RewritePath(a, b, c, false);
                }
            };
        }

        private static bool Authenticate(HttpContext cxt)
        {
            if (cxt.Request.AppRelativeCurrentExecutionFilePath.Contains("Logins")) return true;
            if (cxt.Request.Headers["Authorization"] == null) return false;//false
            else {
                BaseDato.AgregarParametro("@token", cxt.Request.Headers["Authorization"].ToString());
                BaseDato.Ejecutar("select * from acceso where token=@token", BaseDato.TipoEjecucion.DevuelveTable, System.Data.CommandType.Text);
                if (BaseDato.EsError) { return false; }
                if (BaseDato.Tabla.Rows.Count!=1) { return false; }
                return true;
            }
        }
        private static BaseDato basedato = null;
        private static BaseDato BaseDato
        {
            get
            {
                if (basedato == null)
                    basedato = new BaseDato();
                return basedato;
            }
        }

    }
}
