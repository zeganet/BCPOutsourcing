﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCPDominio;
using NHibernate;
using BCPDominio;

namespace BCPServices.Persistencia
{
    public class AreaDAO : BaseDAO<Area, int>
    {
        Area OtroMetodo(Area entidad)
        {
            using (ISession sesion = NHibernateHelper.ObtenerSesion())
            {
                sesion.Save(entidad);
                sesion.Flush();
            }
            return entidad;
        }
    }
}