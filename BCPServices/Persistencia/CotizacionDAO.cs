﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCPDominio;
using NHibernate;
using NHibernate.Criterion;

namespace BCPServices.Persistencia
{
    public class CotizacionDAO : BaseDAO<Cotizacion, int>
    {
        public ICollection<Cotizacion> Listar()
        {
            using (ISession sesion = NHibernateHelper.ObtenerSesion())
            {
                ICriteria busqueda = sesion.CreateCriteria(typeof(Cotizacion));
                return busqueda.List<Cotizacion>();
            }
        }

        public Cotizacion ObtenerByPCR(int codigo)
        {
            using (ISession session = NHibernateHelper.ObtenerSesion())
            {
                Cotizacion user = session
                    //.CreateSQLQuery("").
                    .CreateCriteria(typeof(Cotizacion))
                    .Add(Restrictions.Eq("pcr", new PCR() { idpcr = codigo }))
                    .AddOrder(new Order("idcotizacion", false))
                    .SetMaxResults(1)
                    .UniqueResult<Cotizacion>();
                return user;
            }
        }

    }
}