﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCPDominio;
using NHibernate;
using NHibernate.Criterion;
using AutoMapper;
using System.Messaging;

namespace BCPServices.Persistencia
{
    public class PCRDAO : BaseDAO<PCR, int>
    {
        public ICollection<PCR> ByEstado(int codigo,string estado)
        {
            using (ISession session = NHibernateHelper.ObtenerSesion())
            {
                ICriteria pcr = session
                    .CreateCriteria(typeof(PCR))
                    .Add(Restrictions.Eq("estado", new Estado() { idestado = codigo }))
                    .Add(Restrictions.Eq("activo",estado));
                return pcr.List<PCR>();
            }
        }

        public PCR CrearVersion(PCR items)
        {


            BaseDato b = new BaseDato();
            try
            {
                b.IniciarTransaccion();

                b.AgregarParametro("@idrequerimiento", items.requerimiento.idrequerimiento);
                b.AgregarParametro("@idestado", items.estado.idestado);
                b.AgregarParametro("@version", items.version);
                b.AgregarParametro("@detalle", items.detalle);
                b.AgregarParametro("@descripcion", items.descripcion);
                b.AgregarParametro("@idusuarioIBM", items.usuarioIBM.idusuario);
                b.AgregarParametro("@idusuarioOutsourcing", items.usuarioOutsourcing.idusuario);
                b.AgregarParametro("@adjuntotecnico", items.adjuntotecnico);
                b.AgregarParametro("@fecharegistro", items.fecharegistro);
                b.AgregarParametro("@activo", items.activo);

                b.EjecutarTransaccion("OUT_PCR_Insert");
                if (b.EsError) { throw new ArgumentException(b.Resultado); }
                items.idpcr = int.Parse(b.Resultado.ToString());

                string rutaColaOut = @".\private$\BCPMensaje";
                if (!MessageQueue.Exists(rutaColaOut))
                    MessageQueue.Create(rutaColaOut);
                MessageQueue colaOut = new MessageQueue(rutaColaOut);
                Message mensajeOut = new Message();
                mensajeOut.Label = "Nuevo Mensaje";
                mensajeOut.Body = items ;
                colaOut.Send(mensajeOut);


                b.ConfirmarTransaccion();
            }
            catch (Exception)
            {
                b.CancelarTransaccion();
                throw;
            }
            
            

            //using (ISession sesion = NHibernateHelper.ObtenerSesion())
            //{
            //    var t = sesion.BeginTransaction();
            //    try
            //    {
            //        t.Begin();
            //        sesion.CreateSQLQuery("update pcr set activo='I' where idrequerimiento= " + items.requerimiento.idrequerimiento + "").ExecuteUpdate();


            //        sesion.Save(items);
            //        sesion.Flush();

            //        sesion.CreateSQLQuery("update requerimiento set estado='I' where idrequerimiento= " + items.requerimiento.idrequerimiento  + "").ExecuteUpdate();

                    
                    
            //        t.Commit();
            //    }
            //    catch (Exception)
            //    {
            //        t.Rollback();
            //        throw;
            //    }
            //}
            return items;
        }


        public void EliminarPCR(PCR items)
        {
            using (ISession sesion = NHibernateHelper.ObtenerSesion())
            {
                var t = sesion.BeginTransaction();
                try
                {
                    t.Begin();

                    sesion.CreateSQLQuery("update pcr set activo='I' where idpcr= " + items.idpcr + "").ExecuteUpdate();
                    //sesion.Delete(items);
                    //sesion.Flush();

                    sesion.CreateSQLQuery("update requerimiento set estado='AO' where idrequerimiento= " + items.requerimiento.idrequerimiento + "").ExecuteUpdate();

                    t.Commit();
                }
                catch (Exception e)
                {
                    t.Rollback();
                    throw;
                }
            }
        }

    }
}