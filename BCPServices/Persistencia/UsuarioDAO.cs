﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCPDominio;
using NHibernate;
using NHibernate.Criterion;


namespace BCPServices.Persistencia
{
    public class UsuarioDAO : BaseDAO<Usuario, int>
    {
        public Usuario GetByUserName(Usuario items)
        {
            using (ISession session = NHibernateHelper.ObtenerSesion())
            {
                Usuario user = session
                    .CreateCriteria(typeof(Usuario))
                    .Add(Restrictions.Eq("usuario", items.usuario))
                    .Add(Restrictions.Eq("estado", "A"))
                    .UniqueResult<Usuario>();
                return user;
            }
        }

    }
}