﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCPDominio;
using NHibernate;
using BCPDominio;
using NHibernate.Criterion;

namespace BCPServices.Persistencia
{
    public class RequerimientoDAO : BaseDAO<Requerimiento, int>
    {
        public ICollection<Requerimiento> byEstado(string estado)
        {
            using (ISession session = NHibernateHelper.ObtenerSesion())
            {
                ICriteria requerimiento = session
                    .CreateCriteria(typeof(Requerimiento))
                    .Add(Restrictions.Eq("estado", estado));
                return requerimiento.List<Requerimiento>();
            }
        }
    }
}