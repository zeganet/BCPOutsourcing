﻿using BCPDominio;
using BCPLoginServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BCPServices
{
    [ServiceContract]
    public interface ILogins
    {
        [OperationContract]
        login ValidarSession(Usuario usuario);
    }
}
