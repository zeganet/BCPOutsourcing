﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BCPDominio;
using BCPServices.Persistencia;
using System.ServiceModel.Web;

namespace BCPServices
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "PCRs" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione PCRs.svc o PCRs.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Estados : IEstados
    {
        #region "Fabricas"
        private EstadoDAO estadoDAO = null;
        private EstadoDAO EstadoDAO
        {
            get
            {
                if (estadoDAO == null)
                    estadoDAO = new EstadoDAO();
                return estadoDAO;
            }
        }
        #endregion

        public Estado Read(string codigo)
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotImplemented;
            ctx.OutgoingResponse.StatusDescription = "El metodo no esta implementado por medidas de seguridad";
            return null;
        }

        public List<Estado> ReadAll()
        {
            return EstadoDAO.ListarTodos().ToList();
        }

        public Estado Create(Estado items)
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotImplemented;
            ctx.OutgoingResponse.StatusDescription = "El metodo no esta implementado por medidas de seguridad";
            return null;
        }

        public Estado Update(Estado items)
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotImplemented;
            ctx.OutgoingResponse.StatusDescription = "El metodo no esta implementado por medidas de seguridad";
            return null;
        }

        public void Delete(Estado items)
        {
            WebOperationContext ctx = WebOperationContext.Current;
            ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotImplemented;
            ctx.OutgoingResponse.StatusDescription = "El metodo no esta implementado por medidas de seguridad";
        }
    }
}
