﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BCPLoginServices
{

    [ServiceContract]
    public interface ILoginService
    {
        [OperationContract]
        BCPLoginServices.login ValidarSession(string usuario, string clave);
    }

    [DataContract]
    public class login
    {

        public login(bool success,string mensaje) {
            this.Success = success;
            this.Mensaje = mensaje;
        }

        bool success = false;
        int idusuario;
        string usuario = "";
        string mensaje = "";
        string oauth_token = "";

        [DataMember]
        public bool Success
        {
            get { return success; }
            set { success = value; }
        }
        [DataMember]
        public int Idusuario
        {
            get { return idusuario; }
            set { idusuario = value; }
        }
        [DataMember]
        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        [DataMember]
        public string Mensaje
        {
            get { return mensaje; }
            set { mensaje = value; }
        }
        [DataMember]
        public string Oauth_token
        {
            get { return oauth_token; }
            set { oauth_token = value; }
        }
        [DataMember]
        public string Hora
        {
            get { return DateTime.Now.ToShortTimeString(); }
            set { }
        }
    }
}
