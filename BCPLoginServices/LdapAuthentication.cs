﻿using System;
using System.Text;
using System.Collections;
using System.DirectoryServices;

namespace BCPLoginServices
{
    public class LdapAuthentication
    {
        private string _path;
        private string _filterAttribute;

        public LdapAuthentication()
        {
            //_path = "LDAP://DC=upc,DC=edu,DC=pe";
            _path = "LDAP://DC=digesa,DC=minsa,DC=gob,DC=pe";
        }

        public login IsAuthenticated(string username, string pwd)
        {
            //string domainAndUsername = domain + @"\" + username;
            string domainAndUsername = username;
            DirectoryEntry entry = new DirectoryEntry(_path, domainAndUsername, pwd);

            try
            {
                object obj = entry.NativeObject;

                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(SAMAccountName=" + username + ")";
                search.PropertiesToLoad.Add("cn");
                search.PropertiesToLoad.Add("memberOf");
                search.PropertiesToLoad.Add("distinguishedName");
                search.PropertiesToLoad.Add("displayName");
                search.PropertiesToLoad.Add("mail");
                search.PropertiesToLoad.Add("CN");
                search.PropertiesToLoad.Add("Title");
                search.PropertiesToLoad.Add("sn");
                search.PropertiesToLoad.Add("givenname");
                search.PropertiesToLoad.Add("telephoneNumber");



                search.PropertiesToLoad.Add("accountExpires");
                search.PropertiesToLoad.Add("adminCount");
                search.PropertiesToLoad.Add("appSchemaVersion");
                search.PropertiesToLoad.Add("auditingPolicy");
                search.PropertiesToLoad.Add("badPasswordTime");
                search.PropertiesToLoad.Add("badPwdCount");
                search.PropertiesToLoad.Add("c");
                search.PropertiesToLoad.Add("cn");
                search.PropertiesToLoad.Add("co");
                search.PropertiesToLoad.Add("codePage");
                search.PropertiesToLoad.Add("cOMClassID");
                search.PropertiesToLoad.Add("comment");
                search.PropertiesToLoad.Add("company");
                search.PropertiesToLoad.Add("countryCode");
                search.PropertiesToLoad.Add("creationTime");
                search.PropertiesToLoad.Add("dc");
                search.PropertiesToLoad.Add("deliveryMechanism");
                search.PropertiesToLoad.Add("department");
                search.PropertiesToLoad.Add("description");
                search.PropertiesToLoad.Add("directReports");
                search.PropertiesToLoad.Add("displayName");
                search.PropertiesToLoad.Add("distinguishedName");
                search.PropertiesToLoad.Add("DN");
                search.PropertiesToLoad.Add("dNSHostName");
                search.PropertiesToLoad.Add("dNSProperty");
                search.PropertiesToLoad.Add("dnsRecord");
                search.PropertiesToLoad.Add("dNSTombstoned");
                search.PropertiesToLoad.Add("driverName");
                search.PropertiesToLoad.Add("driverVersion");
                search.PropertiesToLoad.Add("dSCorePropagationData");
                search.PropertiesToLoad.Add("extensionName");
                search.PropertiesToLoad.Add("flags");
                search.PropertiesToLoad.Add("forceLogoff");
                search.PropertiesToLoad.Add("frsComputerReference");
                search.PropertiesToLoad.Add("frsComputerReferenceBL");
                search.PropertiesToLoad.Add("fRSFileFilter");
                search.PropertiesToLoad.Add("fRSMemberReference");
                search.PropertiesToLoad.Add("fRSMemberReferenceBL");
                search.PropertiesToLoad.Add("fRSPrimaryMember");
                search.PropertiesToLoad.Add("fRSReplicaSetGUID");
                search.PropertiesToLoad.Add("fRSReplicaSetType");
                search.PropertiesToLoad.Add("fRSRootPath");
                search.PropertiesToLoad.Add("fRSStagingPath");
                search.PropertiesToLoad.Add("fRSVersionGUID");
                search.PropertiesToLoad.Add("fRSWorkingPath");
                search.PropertiesToLoad.Add("fSMORoleOwner");
                search.PropertiesToLoad.Add("givenName");
                search.PropertiesToLoad.Add("gPCFileSysPath");
                search.PropertiesToLoad.Add("gPCFunctionalityVersion");
                search.PropertiesToLoad.Add("gPCMachineExtensionNames");
                search.PropertiesToLoad.Add("gPCUserExtensionNames");
                search.PropertiesToLoad.Add("gPLink");
                search.PropertiesToLoad.Add("gPOptions");
                search.PropertiesToLoad.Add("groupType");
                search.PropertiesToLoad.Add("homeMDB");
                search.PropertiesToLoad.Add("homeMTA");
                search.PropertiesToLoad.Add("homePhone");
                search.PropertiesToLoad.Add("info");
                search.PropertiesToLoad.Add("initials");
                search.PropertiesToLoad.Add("installUiLevel");
                search.PropertiesToLoad.Add("instanceType");
                search.PropertiesToLoad.Add("internetEncoding");
                search.PropertiesToLoad.Add("ipsecData");
                search.PropertiesToLoad.Add("ipsecDataType");
                search.PropertiesToLoad.Add("ipsecFilterReference");
                search.PropertiesToLoad.Add("ipsecID");
                search.PropertiesToLoad.Add("ipsecISAKMPReference");
                search.PropertiesToLoad.Add("ipsecName");
                search.PropertiesToLoad.Add("iPSECNegotiationPolicyAction");
                search.PropertiesToLoad.Add("ipsecNegotiationPolicyReference");
                search.PropertiesToLoad.Add("iPSECNegotiationPolicyType");
                search.PropertiesToLoad.Add("ipsecNFAReference");
                search.PropertiesToLoad.Add("ipsecOwnersReference");
                search.PropertiesToLoad.Add("isCriticalSystemObject");
                search.PropertiesToLoad.Add("keywords");
                search.PropertiesToLoad.Add("l");
                search.PropertiesToLoad.Add("lastLogoff");
                search.PropertiesToLoad.Add("lastLogon");
                search.PropertiesToLoad.Add("lastLogonTimestamp");
                search.PropertiesToLoad.Add("lastSetTime");
                search.PropertiesToLoad.Add("lastUpdateSequence");
                search.PropertiesToLoad.Add("legacyExchangeDN");
                search.PropertiesToLoad.Add("localeID");
                search.PropertiesToLoad.Add("localPolicyFlags");
                search.PropertiesToLoad.Add("location");
                search.PropertiesToLoad.Add("lockoutDuration");
                search.PropertiesToLoad.Add("lockOutObservationWindow");
                search.PropertiesToLoad.Add("lockoutThreshold");
                search.PropertiesToLoad.Add("logonCount");
                search.PropertiesToLoad.Add("logonHours");
                search.PropertiesToLoad.Add("machineArchitecture");
                search.PropertiesToLoad.Add("mail");
                search.PropertiesToLoad.Add("mailNickname");
                search.PropertiesToLoad.Add("managedBy");
                search.PropertiesToLoad.Add("managedObjects");
                search.PropertiesToLoad.Add("manager");
                search.PropertiesToLoad.Add("mAPIRecipient");
                search.PropertiesToLoad.Add("masteredBy");
                search.PropertiesToLoad.Add("maxPwdAge");
                search.PropertiesToLoad.Add("mDBUseDefaults");
                search.PropertiesToLoad.Add("member");
                search.PropertiesToLoad.Add("memberOf");
                search.PropertiesToLoad.Add("middleName");
                search.PropertiesToLoad.Add("minPwdAge");
                search.PropertiesToLoad.Add("minPwdLength");
                search.PropertiesToLoad.Add("mobile");
                search.PropertiesToLoad.Add("modifiedCount");
                search.PropertiesToLoad.Add("modifiedCountAtLastProm");
                search.PropertiesToLoad.Add("msDS-AllowedToDelegateTo");
                search.PropertiesToLoad.Add("msDS-AllUsersTrustQuota");
                search.PropertiesToLoad.Add("msDS-Behavior-Version");
                search.PropertiesToLoad.Add("mS-DS-CreatorSID");
                search.PropertiesToLoad.Add("ms-DS-MachineAccountQuota");
                search.PropertiesToLoad.Add("msDs-masteredBy");
                search.PropertiesToLoad.Add("msDS-PerUserTrustQuota");
                search.PropertiesToLoad.Add("msDS-PerUserTrustTombstonesQuota");
                search.PropertiesToLoad.Add("msDS-TombstoneQuotaFactor");
                search.PropertiesToLoad.Add("msExchALObjectVersion");
                search.PropertiesToLoad.Add("msExchExpansionServerName");
                search.PropertiesToLoad.Add("msExchHideFromAddressLists");
                search.PropertiesToLoad.Add("msExchHomeServerName");
                search.PropertiesToLoad.Add("msExchMailboxGuid");
                search.PropertiesToLoad.Add("msExchMailboxSecurityDescriptor");
                search.PropertiesToLoad.Add("msExchMasterAccountSid");
                search.PropertiesToLoad.Add("msExchMobileMailboxFlags");
                search.PropertiesToLoad.Add("msExchPFTreeType");
                search.PropertiesToLoad.Add("msExchPoliciesIncluded");
                search.PropertiesToLoad.Add("msExchRecipientDisplayType");
                search.PropertiesToLoad.Add("msExchRecipientTypeDetails");
                search.PropertiesToLoad.Add("msExchRequireAuthToSendTo");
                search.PropertiesToLoad.Add("msExchUserAccountControl");
                search.PropertiesToLoad.Add("msExchUserCulture");
                search.PropertiesToLoad.Add("msExchVersion");
                search.PropertiesToLoad.Add("msieee80211-Data");
                search.PropertiesToLoad.Add("msieee80211-DataType");
                search.PropertiesToLoad.Add("msieee80211-ID");
                search.PropertiesToLoad.Add("msiFileList");
                search.PropertiesToLoad.Add("msiScriptName");
                search.PropertiesToLoad.Add("msiScriptPath");
                search.PropertiesToLoad.Add("mSMQDependentClientServices");
                search.PropertiesToLoad.Add("mSMQDigests");
                search.PropertiesToLoad.Add("mSMQDsServices");
                search.PropertiesToLoad.Add("mSMQEncryptKey");
                search.PropertiesToLoad.Add("mSMQOSType");
                search.PropertiesToLoad.Add("mSMQRoutingServices");
                search.PropertiesToLoad.Add("mSMQServiceType");
                search.PropertiesToLoad.Add("mSMQSignCertificates");
                search.PropertiesToLoad.Add("mSMQSignKey");
                search.PropertiesToLoad.Add("mSMQSites");
                search.PropertiesToLoad.Add("msNPAllowDialin");
                search.PropertiesToLoad.Add("msRTCSIP-ArchiveDefault");
                search.PropertiesToLoad.Add("msRTCSIP-ArchiveDefaultFlags");
                search.PropertiesToLoad.Add("msRTCSIP-ArchiveFederationDefault");
                search.PropertiesToLoad.Add("msRTCSIP-ArchiveFederationDefaultFlags");
                search.PropertiesToLoad.Add("msRTCSIP-BackEndServer");
                search.PropertiesToLoad.Add("msRTCSIP-DefPresenceSubscriptionTimeout");
                search.PropertiesToLoad.Add("msRTCSIP-DefRegistrationTimeout");
                search.PropertiesToLoad.Add("msRTCSIP-DefRoamingDataSubscriptionTimeout");
                search.PropertiesToLoad.Add("msRTCSIP-DomainName");
                search.PropertiesToLoad.Add("msRTCSIP-EnableBestEffortNotify");
                search.PropertiesToLoad.Add("msRTCSIP-EnableFederation");
                search.PropertiesToLoad.Add("msRTCSIP-EnterpriseServices");
                search.PropertiesToLoad.Add("msRTCSIP-FederationEnabled");
                search.PropertiesToLoad.Add("msRTCSIP-FrontEndServers");
                search.PropertiesToLoad.Add("msRTCSIP-GlobalSettingsData");
                search.PropertiesToLoad.Add("msRTCSIP-InternetAccessEnabled");
                search.PropertiesToLoad.Add("msRTCSIP-MaxNumOutstandingSearchPerServer");
                search.PropertiesToLoad.Add("msRTCSIP-MaxNumSubscriptionsPerUser");
                search.PropertiesToLoad.Add("msRTCSIP-MaxPresenceSubscriptionTimeout");
                search.PropertiesToLoad.Add("msRTCSIP-MaxRegistrationTimeout");
                search.PropertiesToLoad.Add("msRTCSIP-MaxRoamingDataSubscriptionTimeout");
                search.PropertiesToLoad.Add("msRTCSIP-MinPresenceSubscriptionTimeout");
                search.PropertiesToLoad.Add("msRTCSIP-MinRegistrationTimeout");
                search.PropertiesToLoad.Add("msRTCSIP-MinRoamingDataSubscriptionTimeout");
                search.PropertiesToLoad.Add("msRTCSIP-NumDevicesPerUser");
                search.PropertiesToLoad.Add("msRTCSIP-OptionFlags");
                search.PropertiesToLoad.Add("msRTCSIP-PoolAddress");
                search.PropertiesToLoad.Add("msRTCSIP-PoolDisplayName");
                search.PropertiesToLoad.Add("msRTCSIP-PoolType");
                search.PropertiesToLoad.Add("msRTCSIP-PoolVersion");
                search.PropertiesToLoad.Add("msRTCSIP-PrimaryHomeServer");
                search.PropertiesToLoad.Add("msRTCSIP-PrimaryUserAddress");
                search.PropertiesToLoad.Add("msRTCSIP-SearchMaxRequests");
                search.PropertiesToLoad.Add("msRTCSIP-SearchMaxResults");
                search.PropertiesToLoad.Add("msRTCSIP-TrustedServerFQDN");
                search.PropertiesToLoad.Add("msRTCSIP-TrustedServerVersion");
                search.PropertiesToLoad.Add("msRTCSIP-UserEnabled");
                search.PropertiesToLoad.Add("name");
                search.PropertiesToLoad.Add("nextRid");
                search.PropertiesToLoad.Add("nTMixedDomain");
                search.PropertiesToLoad.Add("objectCategory");
                search.PropertiesToLoad.Add("objectClass");
                search.PropertiesToLoad.Add("objectGUID");
                search.PropertiesToLoad.Add("objectSid");
                search.PropertiesToLoad.Add("objectVersion");
                search.PropertiesToLoad.Add("operatingSystem");
                search.PropertiesToLoad.Add("operatingSystemServicePack");
                search.PropertiesToLoad.Add("operatingSystemVersion");
                search.PropertiesToLoad.Add("otherHomePhone");
                search.PropertiesToLoad.Add("otherMailbox");
                search.PropertiesToLoad.Add("ou");
                search.PropertiesToLoad.Add("packageFlags");
                search.PropertiesToLoad.Add("packageName");
                search.PropertiesToLoad.Add("packageType");
                search.PropertiesToLoad.Add("pager");
                search.PropertiesToLoad.Add("physicalDeliveryOfficeName");
                search.PropertiesToLoad.Add("portName");
                search.PropertiesToLoad.Add("primaryGroupID");
                search.PropertiesToLoad.Add("printBinNames");
                search.PropertiesToLoad.Add("printCollate");
                search.PropertiesToLoad.Add("printColor");
                search.PropertiesToLoad.Add("printDuplexSupported");
                search.PropertiesToLoad.Add("printEndTime");
                search.PropertiesToLoad.Add("printerName");
                search.PropertiesToLoad.Add("printKeepPrintedJobs");
                search.PropertiesToLoad.Add("printLanguage");
                search.PropertiesToLoad.Add("printMaxResolutionSupported");
                search.PropertiesToLoad.Add("printMaxXExtent");
                search.PropertiesToLoad.Add("printMaxYExtent");
                search.PropertiesToLoad.Add("printMediaReady");
                search.PropertiesToLoad.Add("printMediaSupported");
                search.PropertiesToLoad.Add("printMemory");
                search.PropertiesToLoad.Add("printMinXExtent");
                search.PropertiesToLoad.Add("printMinYExtent");
                search.PropertiesToLoad.Add("printOrientationsSupported");
                search.PropertiesToLoad.Add("printPagesPerMinute");
                search.PropertiesToLoad.Add("printRate");
                search.PropertiesToLoad.Add("printRateUnit");
                search.PropertiesToLoad.Add("printShareName");
                search.PropertiesToLoad.Add("printSpooling");
                search.PropertiesToLoad.Add("printStaplingSupported");
                search.PropertiesToLoad.Add("printStartTime");
                search.PropertiesToLoad.Add("priority");
                search.PropertiesToLoad.Add("priorSetTime");
                search.PropertiesToLoad.Add("productCode");
                search.PropertiesToLoad.Add("protocolSettings");
                search.PropertiesToLoad.Add("proxyAddresses");
                search.PropertiesToLoad.Add("publicDelegates");
                search.PropertiesToLoad.Add("publicDelegatesBL");
                search.PropertiesToLoad.Add("pwdHistoryLength");
                search.PropertiesToLoad.Add("pwdLastSet");
                search.PropertiesToLoad.Add("pwdProperties");
                search.PropertiesToLoad.Add("replUpToDateVector");
                search.PropertiesToLoad.Add("reportToOriginator");
                search.PropertiesToLoad.Add("repsFrom");
                search.PropertiesToLoad.Add("revision");
                search.PropertiesToLoad.Add("rIDAllocationPool");
                search.PropertiesToLoad.Add("rIDAvailablePool");
                search.PropertiesToLoad.Add("rIDManagerReference");
                search.PropertiesToLoad.Add("rIDNextRID");
                search.PropertiesToLoad.Add("rIDPreviousAllocationPool");
                search.PropertiesToLoad.Add("rIDSetReferences");
                search.PropertiesToLoad.Add("rIDUsedPool");
                search.PropertiesToLoad.Add("sAMAccountName");
                search.PropertiesToLoad.Add("sAMAccountType");
                search.PropertiesToLoad.Add("serverName");
                search.PropertiesToLoad.Add("serverReference");
                search.PropertiesToLoad.Add("serverReferenceBL");
                search.PropertiesToLoad.Add("serverState");
                search.PropertiesToLoad.Add("serviceBindingInformation");
                search.PropertiesToLoad.Add("serviceClassName");
                search.PropertiesToLoad.Add("serviceDNSName");
                search.PropertiesToLoad.Add("serviceDNSNameType");
                search.PropertiesToLoad.Add("servicePrincipalName");
                search.PropertiesToLoad.Add("shortServerName");
                search.PropertiesToLoad.Add("showInAddressBook");
                search.PropertiesToLoad.Add("showInAdvancedViewOnly");
                search.PropertiesToLoad.Add("sn");
                search.PropertiesToLoad.Add("st");
                search.PropertiesToLoad.Add("streetAddress");
                search.PropertiesToLoad.Add("subRefs");
                search.PropertiesToLoad.Add("systemFlags");
                search.PropertiesToLoad.Add("targetAddress");
                search.PropertiesToLoad.Add("telephoneNumber");
                search.PropertiesToLoad.Add("textEncodedORAddress");
                search.PropertiesToLoad.Add("title");
                search.PropertiesToLoad.Add("uASCompat");
                search.PropertiesToLoad.Add("uNCName");
                search.PropertiesToLoad.Add("upgradeProductCode");
                search.PropertiesToLoad.Add("url");
                search.PropertiesToLoad.Add("userAccountControl");
                search.PropertiesToLoad.Add("userCertificate");
                search.PropertiesToLoad.Add("userParameters");
                search.PropertiesToLoad.Add("userPrincipalName");
                search.PropertiesToLoad.Add("uSNChanged");
                search.PropertiesToLoad.Add("uSNCreated");
                search.PropertiesToLoad.Add("versionNumber");
                search.PropertiesToLoad.Add("versionNumberHi");
                search.PropertiesToLoad.Add("versionNumberLo");
                search.PropertiesToLoad.Add("wellKnownObjects");
                search.PropertiesToLoad.Add("whenChanged");
                search.PropertiesToLoad.Add("whenCreated");
                search.PropertiesToLoad.Add("wWWHomePage");


                SearchResult result = search.FindOne();

                if (null == result)
                {
                    return new login(false, "no se encontraron resultados");
                }
                _path = result.Path;
                //_filterAttribute = (string)result.Properties["cn"][0];
                _filterAttribute = (string)result.Properties["displayName"][0];

                return new login(true, _filterAttribute) { Usuario=username} ;

            }
            catch (Exception ex)
            {
                return new login(false, ex.Message);
            }

            
        }

        public string GetGroups()
        {
            DirectorySearcher search = new DirectorySearcher(_path);
            search.Filter = "(cn=" + _filterAttribute + ")";
            search.PropertiesToLoad.Add("memberOf");
            StringBuilder groupNames = new StringBuilder();

            try
            {
                SearchResult result = search.FindOne();
                int propertyCount = result.Properties["memberOf"].Count;
                string dn;
                int equalsIndex, commaIndex;

                for (int propertyCounter = 0; propertyCounter < propertyCount; propertyCounter++)
                {
                    dn = (string)result.Properties["memberOf"][propertyCounter];
                    equalsIndex = dn.IndexOf("=", 1);
                    commaIndex = dn.IndexOf(",", 1);
                    if (-1 == equalsIndex)
                    {
                        return null;
                    }
                    groupNames.Append(dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1));
                    groupNames.Append("|");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error obtaining group names. " + ex.Message);
            }
            return groupNames.ToString();
        }
    }
}
