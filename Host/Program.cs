﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Host
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServiceHost host = new ServiceHost(typeof(BCPServices.Logins))) {
                host.Open();
                Console.WriteLine("Host iniciado.: ");
                Console.WriteLine("Hora: " + DateTime.Now.ToString());
                Console.WriteLine(host.Authorization.GetType());
                Console.ReadLine();
            }
        }
    }
}
